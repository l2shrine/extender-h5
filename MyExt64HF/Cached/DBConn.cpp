
#include <Cached/DBConn.h>
#include <Common/Utils.h>
#include <sqltypes.h>
#include <sqlext.h>
#include <cstddef>

namespace cached {

DBConn::DBConn() : preparedQuery(0)
{
	reinterpret_cast<DBConn*(*)(DBConn*, int)>(0x45DF84)(this, GetConnectionType());
}

DBConn::~DBConn()
{
	reinterpret_cast<void(*)(DBConn*)>(0x45E13C)(this);
}

void DBConn::Bind(int *i)
{
	reinterpret_cast<void(*)(DBConn*, int*)>(0x45B870)(this, i);
}

void DBConn::Bind(UINT32 *i)
{
	reinterpret_cast<void(*)(DBConn*, UINT32*)>(0x45B7D0)(this, i);
}

void DBConn::Bind(INT64 *i)
{
	reinterpret_cast<void(*)(DBConn*, INT64*)>(0x45B910)(this, i);
}

void DBConn::Bind(wchar_t *buffer, const size_t sizeInBytes)
{
	reinterpret_cast<void(*)(DBConn*, wchar_t*, const size_t)>(0x45B690)(
		this, buffer, sizeInBytes);
}

void DBConn::Unbind()
{
	reinterpret_cast<void(*)(DBConn*)>(0x45C080)(this);
}

void DBConn::Prepare(const wchar_t *query)
{
	SQLPrepareW(handle, const_cast<SQLWCHAR*>(query), SQL_NTS);
	preparedQuery = query;
}

void DBConn::BindParameter(const UINT32 &value)
{
	SQLBindParameter(handle, parameterNo++,
		SQL_PARAM_INPUT, SQL_C_ULONG, SQL_INTEGER, 4, 0,
		const_cast<SQLPOINTER>(reinterpret_cast<const void*>(&value)), 0, 0);
}

void DBConn::BindParameter(const INT32 &value)
{
	SQLBindParameter(handle, parameterNo++,
		SQL_PARAM_INPUT, SQL_C_SLONG, SQL_INTEGER, 4, 0,
		const_cast<SQLPOINTER>(reinterpret_cast<const void*>(&value)), 0, 0);
}

void DBConn::BindParameter(const INT64 &value)
{
	SQLBindParameter(handle, parameterNo++,
		SQL_PARAM_INPUT, SQL_C_SBIGINT, SQL_BIGINT, 8, 0,
		const_cast<SQLPOINTER>(reinterpret_cast<const void*>(&value)), 0, 0);
}

void DBConn::BindParameter(const UINT64 &value)
{
	SQLBindParameter(handle, parameterNo++,
		SQL_PARAM_INPUT, SQL_C_UBIGINT, SQL_BIGINT, 8, 0,
		const_cast<SQLPOINTER>(reinterpret_cast<const void*>(&value)), 0, 0);
}

bool DBConn::Execute()
{
	return reinterpret_cast<bool(*)(DBConn*, const wchar_t*)>(0x45CC14)(
		this, preparedQuery);
}

bool DBConn::Fetch()
{
	return reinterpret_cast<bool(*)(DBConn*)>(0x45B4E4)(this);
}

static void check()
{
	static_assert(offsetof(DBConn, preparedQuery) == 0x480);
}

} // namespace cached

