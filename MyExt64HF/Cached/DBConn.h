
#pragma once

#include <Windows.h>
#include <sqltypes.h>

namespace cached {

class DBConn {
public:
	DBConn();
	~DBConn();

	static void Test();

	void Bind(int *i);
	void Bind(UINT32 *i);
	void Bind(INT64 *i);
	void Bind(wchar_t *buffer, const size_t sizeInBytes);

	void Unbind();

	void Prepare(const wchar_t *query);

	void BindParameter(const UINT32 &value);
	void BindParameter(const INT32 &value);
	void BindParameter(const UINT64 &value);
	void BindParameter(const INT64 &value);

	template<class T>
	void BindParameter(const T *value);

	template<>
	void BindParameter(const wchar_t *value)
	{
		reinterpret_cast<void(*)(DBConn*, const wchar_t*)>(0x403E98)(this, value);
	}

	bool Execute();
	bool Fetch();

	/* 0x0000 */ unsigned char padding0x0000[0x0030 - 0x0000];
	/* 0x0030 */ SQLHSTMT handle;
	/* 0x0038 */ unsigned char padding0x0038[0x004A - 0x0038];
	/* 0x004A */ UINT16 parameterNo;
	/* 0x004C */ unsigned char padding0x004C[0x0480 - 0x004C];
	/* 0x0480 */ // ext begin
	const wchar_t *preparedQuery;
};

} // namespace cached

