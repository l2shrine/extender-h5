
#pragma once

namespace npc {

class CServerSocket;

class NPCServer {
public:
	static NPCServer* Instance();
	void Send(const char *format, ...);

	/* 0x0000 */ unsigned char padding0x0000[0x3828 - 0x0000];
	/* 0x3828 */ CServerSocket *socket;
};

} // namespace npc

