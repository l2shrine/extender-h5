
#pragma once

#include <NPCd/NPCFunction.h>

namespace npc {

class GlobalObject_BroadcastRedSky : public NPCFunction {
public:
	GlobalObject_BroadcastRedSky();
	virtual void* Call(void *caller, void **params);
	virtual void SetTypes();
	static int BroadcastRedSky(void*, int duration);
};

} // namespace npc

