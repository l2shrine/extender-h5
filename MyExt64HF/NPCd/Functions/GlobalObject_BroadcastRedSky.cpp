
#include <NPCd/Functions/GlobalObject_BroadcastRedSky.h>
#include <NPCd/NPCServer.h>
#include <Common/CLog.h>
#include <Common/Enum.h>

namespace npc {

GlobalObject_BroadcastRedSky::GlobalObject_BroadcastRedSky() :
	NPCFunction(L"BroadcastRedSky", &BroadcastRedSky)
{
}

void* GlobalObject_BroadcastRedSky::Call(void *caller, void **params)
{
	return reinterpret_cast<void*(*)(void*, void*)>(functionPtr.functionPtr)(
		caller, params[0]);
}

void GlobalObject_BroadcastRedSky::SetTypes()
{
	SetReturnType(Type::TYPE_VOID);
	AddParameter(Type::TYPE_INT);
}

int GlobalObject_BroadcastRedSky::BroadcastRedSky(void*, int duration)
{
	NPCServer::Instance()->Send("chd", 0x3A, NpcExtPacket::BROADCAST_RED_SKY, duration);
	return 0;
}

} // namespace npc

