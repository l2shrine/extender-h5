
#include <NPCd/Functions/CNPC_ClearContributeData.h>
#include <NPCd/NPCServer.h>
#include <Common/CLog.h>
#include <Common/Enum.h>

namespace npc {

CNPC_ClearContributeData::CNPC_ClearContributeData() :
	NPCFunction(L"ClearContributeData", &ClearContributeData)
{
}

void* CNPC_ClearContributeData::Call(void *caller, void **params)
{
	return reinterpret_cast<void*(*)(void*)>(functionPtr.functionPtr)(
		caller);
}

void CNPC_ClearContributeData::SetTypes()
{
	SetReturnType(Type::TYPE_VOID);
}

int CNPC_ClearContributeData::ClearContributeData(CNPC *npc)
{
	NPCServer::Instance()->Send("chd", 0x3A, NpcExtPacket::CLEAR_CONTRIBUTE_DATA, npc->sd->index);
	return 0;
}

} // namespace npc

