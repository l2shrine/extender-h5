
#pragma once

#include <NPCd/NPCFunction.h>
#include <NPCd/CNPC.h>

namespace npc {

class CNPC_IsPremium : public NPCFunction {
public:
	CNPC_IsPremium();
	virtual void* Call(void *caller, void **params);
	virtual void SetTypes();
	static int IsPremium(CNPC *npc, CSharedCreatureData *talker);
};

} // namespace npc

