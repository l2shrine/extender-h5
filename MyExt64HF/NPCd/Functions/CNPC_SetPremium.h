
#pragma once

#include <NPCd/NPCFunction.h>
#include <NPCd/CNPC.h>

namespace npc {

class CNPC_SetPremium : public NPCFunction {
public:
	CNPC_SetPremium();
	virtual void* Call(void *caller, void **params);
	virtual void SetTypes();
	static int SetPremium(CNPC *npc, CSharedCreatureData *talker, int seconds);
};

} // namespace npc

