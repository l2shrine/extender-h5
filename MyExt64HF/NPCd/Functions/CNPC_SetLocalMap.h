
#pragma once

#include <NPCd/NPCFunction.h>
#include <NPCd/CNPC.h>

namespace npc {

class CNPC_SetLocalMap : public NPCFunction {
public:
	CNPC_SetLocalMap();
	virtual void* Call(void *caller, void **params);
	virtual void SetTypes();
	static int SetLocalMap(CNPC *npc, int mapId, int key, int value);
};

} // namespace npc

