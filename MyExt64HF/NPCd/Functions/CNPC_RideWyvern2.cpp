
#include <NPCd/Functions/CNPC_RideWyvern2.h>
#include <NPCd/NPCServer.h>
#include <Common/CLog.h>
#include <Common/Enum.h>

namespace npc {

CNPC_RideWyvern2::CNPC_RideWyvern2() :
	NPCFunction(L"RideWyvern2", &RideWyvern2)
{
}

void* CNPC_RideWyvern2::Call(void *caller, void **params)
{
	return reinterpret_cast<void*(*)(void*, void*, void*, void*)>(functionPtr.functionPtr)(
		caller, params[0], params[1], params[2]);
}

void CNPC_RideWyvern2::SetTypes()
{
	SetReturnType(Type::TYPE_VOID);
	AddParameter(Type::TYPE_CREATURE);
	AddParameter(Type::TYPE_INT);
	AddParameter(Type::TYPE_INT);
}

int CNPC_RideWyvern2::RideWyvern2(CNPC *npc, CSharedCreatureData *talker, int classId, int duration)
{
	NPCServer::Instance()->Send("chddd", 0x3A, NpcExtPacket::RIDE_WYVERN2, talker->index, classId, duration);
	return 0;
}

} // namespace npc

