
#include <NPCd/NPCFunction.h>
#include <NPCd/Functions/CNPC_RideWyvern2.h>
#include <NPCd/Functions/CNPC_SetLocalMap.h>
#include <NPCd/Functions/CNPC_GetLocalMap.h>
#include <NPCd/Functions/CNPC_IsPvpOrPk.h>
#include <NPCd/Functions/CNPC_ClearContributeData.h>
#include <NPCd/Functions/CNPC_SetPremium.h>
#include <NPCd/Functions/CNPC_IsPremium.h>
#include <NPCd/Functions/GlobalObject_DebugLog.h>
#include <NPCd/Functions/GlobalObject_BroadcastAnnounce.h>
#include <NPCd/Functions/GlobalObject_BroadcastRedSky.h>
#include <Common/Utils.h>

namespace npc {

void NPCFunction::AddFunctions()
{
	AddFunction<Type::TYPE_NPC, CNPC_RideWyvern2>();
	AddFunction<Type::TYPE_NPC, CNPC_SetLocalMap>();
	AddFunction<Type::TYPE_NPC, CNPC_GetLocalMap>();
	AddFunction<Type::TYPE_NPC, CNPC_IsPvpOrPk>();
	AddFunction<Type::TYPE_NPC, CNPC_ClearContributeData>();
	AddFunction<Type::TYPE_NPC, CNPC_SetPremium>();
	AddFunction<Type::TYPE_NPC, CNPC_IsPremium>();
	AddFunction<Type::TYPE_GLOBALOBJECT, GlobalObject_DebugLog>();
	AddFunction<Type::TYPE_GLOBALOBJECT, GlobalObject_BroadcastAnnounce>();
	AddFunction<Type::TYPE_GLOBALOBJECT, GlobalObject_BroadcastRedSky>();
}

void NPCFunction::Init()
{
	WriteMemoryQWORD(0x5EA3F8 , reinterpret_cast<UINT32>(reinterpret_cast<void(*)()>(
		RegisterFunctions<0x4BD724, Type::TYPE_NPC>)));

	WriteMemoryQWORD(0x5B4418, reinterpret_cast<UINT32>(reinterpret_cast<void(*)()>(
		RegisterFunctions<0x445AF8, Type::TYPE_GLOBALOBJECT>)));

	AddFunctions();
}

std::list<NPCFunction*> NPCFunction::functionsToRegister[Type::TYPES_END];

NPCFunction::NPCFunction(const wchar_t *name, void *functionPtr)
{
	memset(padding0x0008, 0, 8);
	memset(padding0x0048, 0, 0x18);
	this->name = name;
	this->functionPtr.functionPtr = functionPtr;
}

NPCFunction::~NPCFunction()
{
}

void* NPCFunction::CallFn(void *caller, void **params)
{
	UINT64 caller_ = static_cast<UINT64>(static_cast<INT64>(functionPtr.unknown));
	caller_ += reinterpret_cast<UINT64>(caller);
	return Call(reinterpret_cast<void*>(caller_), params);
}

void NPCFunction::SetReturnType(Type::TypeID type)
{
	UINT32 *flag = reinterpret_cast<UINT32*>(0x69DF00);
	if (!(*flag & 1)) {
		*flag |= 1;
		reinterpret_cast<void(*)(UINT64)>(0x506920)(0x69DDE0);
	}
	returnType = *reinterpret_cast<void**(*)(UINT64, Type::TypeID)>(0x412370)(0x69DDE0, type);
}

void NPCFunction::AddParameter(Type::TypeID type)
{
	UINT32 *flag = reinterpret_cast<UINT32*>(0x69DF00);
	if (!(*flag & 1)) {
		*flag |= 1;
		reinterpret_cast<void(*)(UINT64)>(0x506920)(0x69DDE0);
	}
	void *parameter = *reinterpret_cast<void**(*)(UINT64, Type::TypeID)>(0x412370)(0x69DDE0, type);
	reinterpret_cast<void(*)(void**, void*)>(0x439678)(&parameters, &parameter);
}

void NPCFunction::Register(void *registry)
{
	SetTypes();
	reinterpret_cast<void(*)(void*, NPCFunction*)>(0x5081D0)(registry, this);
}

NPCFunctionPtr::NPCFunctionPtr()
{
	memset(this, 0, sizeof(*this));
}

CompileTimeOffsetCheck(NPCFunctionPtr, unknown, 0x8);
CompileTimeOffsetCheck(NPCFunction, name, 0x10);
CompileTimeOffsetCheck(NPCFunction, padding0x0048, 0x48);
CompileTimeOffsetCheck(NPCFunction, functionPtr, 0x60);

} // namespace npc

