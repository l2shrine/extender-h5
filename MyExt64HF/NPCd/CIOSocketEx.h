
#pragma once

#include <NPCd/CIOObject.h>
#include <stdarg.h>

namespace npc {

class CIOSocketEx : public CIOObject {
public:
	/* 0x0060 */ virtual void Initialize() {}
	/* 0x0068 */ virtual void OnCreate() {}
	/* 0x0070 */ virtual bool vfn0x0070() { return false; }
	/* 0x0078 */ virtual bool vfn0x0078() { return false; }
	/* 0x0080 */ virtual void Send(const char *, ...) {}
	/* 0x0088 */ virtual void SendV(const char *, va_list va) {}
	/* 0x0090 */ virtual void GracefulClose() {}
	/* 0x0098 */ virtual void vfn0x0098() {}
	/* 0x00A0 */ virtual void vfn0x00A0() {}

	/* 0x0018 */ unsigned char padding0x0018[0x00B0 - 0x0018];
	/* 0x00B8 */
};

} // namespace npc

