
#pragma once

#include <NPCd/CObject.h>
#include <Windows.h>

class CSharedCreatureData;

namespace npc {

class CCreature : public CObject {
public:
	/* 0x0068 */ virtual bool IsUser_0x0068() { return false; }
	/*        */ inline bool IsUser() { return IsUser_0x0068(); }
	/* 0x0070 */ virtual bool IsNpc_0x0070() { return false; }
	/*        */ inline bool IsNpc() { return IsNpc_0x0070(); }
	/* 0x0078 */ virtual void vfn0x0078() {}
	/* 0x0080 */ virtual void vfn0x0080() {}
	/* 0x0088 */ virtual void vfn0x0088() {}
	/* 0x0090 */ virtual bool IsTargetable_0x0090() { return false; }
	/*        */ inline bool IsTargetable() { return IsTargetable_0x0090(); }
	/* 0x0098 */ virtual bool AddReverseHate_0x0098(unsigned int, int) { return false; }
	/*        */ inline bool AddReverseHate(unsigned int i, int j) { return AddReverseHate_0x0098(i, j); }
	/* 0x00A0 */ virtual void SeeCreature_0x00A0(CCreature*) {}
	/*        */ inline void SeeCreature(CCreature *creature) { SeeCreature_0x00A0(creature); }
	/* 0x00A8 */ virtual void Spelled_0x00A8(CSharedCreatureData*, int, int) {}
	/*        */ inline void Spelled(CSharedCreatureData *caster, int i, int j) { Spelled_0x00A8(caster, i, j); }
	/* 0x00B0 */ virtual void vfn0x00B0() {}

	/* 0x0020 */ CSharedCreatureData *sd;
	/* 0x0028 */
};

} // namespace npc

