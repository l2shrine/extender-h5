
#pragma once

#include <Server/CWorldObject.h>
#include <Server/CInventory.h>
#include <Common/CSPointer.h>
#include <Common/Enum.h>
#include <Common/xstd.h>

class CSharedCreatureData;
class CSharedItemData;

namespace l2server {

class ItemOptionKey;
class BaseAttribute;
class CItem;
class CUserSocket;
class CContributeData;
class CCreatureController;

class CCreature : public CWorldObject {
public:
	static const UINT64 vtable = 0xBC89E8;

	enum PathFindType {
	};

	/* 0x04F0 */ virtual bool SetVisibleBySkill_0x04F0(bool visible) { return false; }
	/*        */ inline bool SetVisibleBySkill(bool visible) { return SetVisibleBySkill_0x04F0(visible); }
	/* 0x04F8 */ virtual void vfn0x04F8() {}
	/* 0x0500 */ virtual void vfn0x0500() {}
	/* 0x0508 */ virtual void OnCreate_0x0508() {}
	/*        */ inline void OnCreate() { OnCreate_0x0508(); }
	/* 0x0510 */ virtual void vfn0x0510() {}
	/* 0x0518 */ virtual void OnCreateFakeNPC_0x0518() {}
	/*        */ inline void OnCreateFakeNPC() { OnCreateFakeNPC_0x0518(); }
	/* 0x0520 */ virtual void Delete_0x0520() {}
	/*        */ inline void Delete() { Delete_0x0520(); }
	/* 0x0528 */ virtual void vfn0x0528() {}
	/* 0x0530 */ virtual void vfn0x0530() {}
	/* 0x0538 */ virtual void vfn0x0538() {}
	/* 0x0540 */ virtual bool ChangeClass_0x0540(Class class_) { return false; }
	/*        */ inline bool ChangeClass(Class class_) { return ChangeClass_0x0540(class_); }
	/* 0x0548 */ virtual void vfn0x0548() {}
	/* 0x0550 */ virtual void vfn0x0550() {}
	/* 0x0558 */ virtual void vfn0x0558() {}
	/* 0x0560 */ virtual void vfn0x0560() {}
	/* 0x0568 */ virtual AttackType GetAttackType_0x0568(CSharedItemData **items) { return AttackTypeNone; }
	/*        */ inline AttackType GetAttackType(CSharedItemData **items) { return GetAttackType_0x0568(items); }
	/* 0x0570 */ virtual AttackType GetRealAttackType_0x0570() { return AttackTypeNone;  }
	/*        */ inline AttackType GetRealAttackType() { return GetRealAttackType_0x0570(); }
	/* 0x0578 */ virtual void vfn0x0578() {}
	/* 0x0580 */ virtual void vfn0x0580() {}
	/* 0x0588 */ virtual void vfn0x0588() {}
	/* 0x0590 */ virtual void vfn0x0590() {}
	/* 0x0598 */ virtual double EquipedArmorsPDefend_0x0598() { return 0.0; }
	/*        */ inline double EquipedArmorsPDefend() { return EquipedArmorsPDefend_0x0598(); }
	/* 0x05A0 */ virtual double EquipedArmorsMpBonus_0x05A0() { return 0.0; }
	/*        */ inline double EquipedArmorsMpBonus() { return EquipedArmorsMpBonus_0x05A0(); }
	/* 0x05A8 */ virtual double EquipedArmorsHpBonus_0x05A8() { return 0.0; }
	/*        */ inline double EquipedArmorsHpBonus() { return EquipedArmorsHpBonus_0x05A8(); }
	/* 0x05B0 */ virtual double EquipedArmorsMDefend_0x05B0() { return 0.0; }
	/*        */ inline double EquipedArmorsMDefend() { return EquipedArmorsMDefend_0x05B0(); }
	/* 0x05B8 */ virtual void vfn0x05B8() {}
	/* 0x05C0 */ virtual INT64 ExpInc_0x05C0(INT64 exp, bool b) { return 0; }
	/*        */ inline INT64 ExpInc(INT64 exp, bool b) { return ExpInc_0x05C0(exp, b); }
	/* 0x05C8 */ virtual void vfn0x05C8() {}
	/* 0x05D0 */ virtual void vfn0x05D0() {}
	/* 0x05D8 */ virtual void vfn0x05D8() {}
	/* 0x05E0 */ virtual void vfn0x05E0() {}
	/* 0x05E8 */ virtual void vfn0x05E8() {}
	/* 0x05F0 */ virtual void vfn0x05F0() {}
	/* 0x05F8 */ virtual void vfn0x05F8() {}
	/* 0x0600 */ virtual void OnGotDamage_0x0600(CCreature *attacker, double d) {}
	/*        */ inline void OnGotDamage(CCreature *attacker, double d) { OnGotDamage_0x0600(attacker, d); }
	/* 0x0608 */ virtual void vfn0x0608() {}
	/* 0x0610 */ virtual void vfn0x0610() {}
	/* 0x0618 */ virtual void vfn0x0618() {}
	/* 0x0620 */ virtual void vfn0x0620() {}
	/* 0x0628 */ virtual void vfn0x0628() {}
	/* 0x0630 */ virtual bool ValidateBaseHP_0x0630() { return false;  }
	/* 0x0638 */ virtual bool ValidatePHit_0x0638() { return false;  }
	/* 0x0640 */ virtual bool ValidatePAvoidRate_0x0640() { return false; }
	/* 0x0648 */ virtual bool ValidatePAttack_0x0648() { return false; }
	/* 0x0650 */ virtual void ValidateMAttack_0x0650() {}
	/* 0x0658 */ virtual bool ValidatePDefend_0x0658() { return false; }
	/* 0x0660 */ virtual bool ValidateShieldDefend_0x0660() { return false; }
	/* 0x0668 */ virtual bool ValidateMDefend_0x0668() { return false; }
	/* 0x0670 */ virtual bool ValidateAttackRange_0x0670() { return false; }
	/* 0x0678 */ virtual void vfn0x0678() {}
	/* 0x0680 */ virtual void ValidatePUseSpeed_0x0680() {}
	/* 0x0688 */ virtual void ValidateMUseSpeed_0x0688() {}
	/* 0x0690 */ virtual bool ValidateArmorType_0x0690() { return false; }
	/* 0x0698 */ virtual bool ValidateOrgAttackSpeed_0x0698() { return false; }
	/* 0x06A0 */ virtual bool ValidateCriticalProb_0x06A0() { return false; }
	/* 0x06A8 */ virtual bool ValidateCarryWeight_0x06A8() { return false; }
	/* 0x06B0 */ virtual void vfn0x06B0() {}
	/* 0x06B8 */ virtual bool ValidateAllOnWeapon_0x06B8() { return false; }
	/* 0x06C0 */ virtual void vfn0x06C0() {}
	/* 0x06C8 */ virtual void vfn0x06C8() {}
	/* 0x06D0 */ virtual void vfn0x06D0() {}
	/* 0x06D8 */ virtual void vfn0x06D8() {}
	/* 0x06E0 */ virtual bool ValidateOrgPAttack_0x06E0() { return false; }
	/* 0x06E8 */ virtual bool ValidateOrgPDefend_0x06E8() { return false; }
	/* 0x06F0 */ virtual bool ValidateOrgMAttack_0x06F0() { return false; }
	/* 0x06F8 */ virtual bool ValidateOrgMDefend_0x06F8() { return false; }
	/* 0x0700 */ virtual void vfn0x0700() {}
	/* 0x0708 */ virtual bool ValidateLevel_0x0708() { return false; }
	/* 0x0710 */ virtual void ValidateAttackSpeedAndTime_0x0710() {}
	/* 0x0718 */ virtual void vfn0x0718() {}
	/* 0x0720 */ virtual void ValidateAttackSpeed_0x0720() {}
	/* 0x0728 */ virtual void vfn0x0728() {}
	/* 0x0730 */ virtual void vfn0x0730() {}
	/* 0x0738 */ virtual void vfn0x0738() {}
	/* 0x0740 */ virtual void vfn0x0740() {}
	/* 0x0748 */ virtual void vfn0x0748() {}
	/* 0x0750 */ virtual void vfn0x0750() {}
	/* 0x0758 */ virtual void vfn0x0758() {}
	/* 0x0760 */ virtual void vfn0x0760() {}
	/* 0x0768 */ virtual void vfn0x0768() {}
	/* 0x0770 */ virtual bool CanUseShot_0x0770(class CSkillInfo *skillInfo) { return false; }
	/* 0x0778 */ virtual void BroadcastWaitType_0x0778(int) {}
	/* 0x0780 */ virtual void BroadcastCombatModeStart_0x0780() {}
	/* 0x0788 */ virtual void BroadcastCombatModeFinish_0x0788() {}
	/* 0x0790 */ virtual bool IsInvisible_0x0790() const { return false; }
	/*        */ inline bool IsInvisible() const { return IsInvisible_0x0790(); }
	/* 0x0798 */ virtual void vfn0x0798() {}
	/* 0x07A0 */ virtual void vfn0x07A0() {}
	/* 0x07A8 */ virtual void InitCoupleAction_0x07A8() {}
	/* 0x07B0 */ virtual void vfn0x07B0() {}
	/* 0x07B8 */ virtual void vfn0x07B8() {}
	/* 0x07C0 */ virtual int EquipItem_0x07C0(CItem *item, SlotType slotType) { return 0; }
	/*        */ inline int EquipItem(CItem *item, SlotType slotType) { return EquipItem_0x07C0(item, slotType); }
	/* 0x07C8 */ virtual SlotType UnequipItem_0x07C8(CItem *item, bool b) { return SlotTypeUnderwear; }
	/*        */ inline SlotType UnequipItem(CItem *item, bool b) { return UnequipItem_0x07C8(item, b); }
	/* 0x07D0 */ virtual SlotType UnequipItem_0x07D0(SlotType slotType, bool b) { return SlotTypeUnderwear; }
	/*        */ inline SlotType UnequipItem(SlotType slotType, bool b) { return UnequipItem_0x07D0(slotType, b); }
	/* 0x07D8 */ virtual void vfn0x07D8() {}
	/* 0x07E0 */ virtual void vfn0x07E0() {}
	/* 0x07E8 */ virtual bool UseItem_0x07E8(CItem &item, int i, bool b) { return false; }
	/*        */ inline bool UseItem(CItem &item, int i, bool b) { return UseItem_0x07E8(item, i, b); }
	/* 0x07F0 */ virtual void UseETCItem_0x07F0(CItem *item, INT64 amount, int i, bool b) {}
	/*        */ inline void UseETCItem(CItem *item, INT64 amount, int i, bool b) { UseETCItem_0x07F0(item, amount, i, b); }
	/* 0x07F8 */ virtual void vfn0x07F8() {}
	/* 0x0800 */ virtual bool GetItem_0x0800(CItem *item) { return false; }
	/*        */ inline bool GetItem(CItem *item) { return GetItem_0x0800(item); }
	/* 0x0808 */ virtual void DeleteAllItemsInInventory_0x0808() {}
	/*        */ inline void DeleteAllItemsInInventory() { DeleteAllItemsInInventory_0x0808(); }
	/* 0x0810 */ virtual int GetInventorySlots_0x0810() { return 0; }
	/*        */ inline int GetInventorySlots() { return GetInventorySlots_0x0810(); }
	/* 0x0818 */ virtual void vfn0x0818() {}
	/* 0x0820 */ virtual void vfn0x0820() {}
	/* 0x0828 */ virtual void vfn0x0828() {}
	/* 0x0830 */ virtual void vfn0x0830() {}
	/* 0x0838 */ virtual void vfn0x0838() {}
	/* 0x0840 */ virtual void vfn0x0840() {}
	/* 0x0848 */ virtual void vfn0x0848() {}
	/* 0x0850 */ virtual void vfn0x0850() {}
	/* 0x0858 */ virtual void SendSystemMessage_0x0858(const wchar_t *sender, const wchar_t *message) {}
	/*        */ inline void SendSystemMessage(const wchar_t *sender, const wchar_t *message) { SendSystemMessage_0x0858(sender, message); }
	/* 0x0860 */ virtual void SendSystemMessage_0x0860(unsigned int id) {}
	/*        */ inline void SendSystemMessage(unsigned int id) { SendSystemMessage_0x0860(id); }
	/* 0x0868 */ virtual void SendSystemMessage_d_0x0868(unsigned int id, int d) {}
	/*        */ inline void SendSystemMessage_d(unsigned int id, int d) { SendSystemMessage_d_0x0868(id, d); }
	/* 0x0870 */ virtual void SendSystemMessage_Q_0x0870(unsigned int id, INT64 q) {}
	/*        */ inline void SendSystemMessage_Q(unsigned int id, INT64 q) { SendSystemMessage_Q_0x0870(id, q); }
	/* 0x0878 */ virtual void SendSystemMessageWithSelf_0x0878(unsigned int id) {}
	/*        */ inline void SendSystemMessageWithSelf(unsigned int id) { SendSystemMessageWithSelf_0x0878(id); }
	/* 0x0880 */ virtual bool AddItemToInventory_0x0880(struct InventoryParams &params) { return false; }
	/*        */ inline bool AddItemToInventory(struct InventoryParams &params) { return AddItemToInventory_0x0880(params); }
	/* 0x0888 */ virtual bool AddItemToInventory2_0x0888(CItem *item) { return false; }
	/*        */ inline bool AddItemToInventory2(CItem *item) { return AddItemToInventory2_0x0888(item); }
	/* 0x0890 */ virtual bool DeleteItemInInventory_0x0890(int, INT64, class AtomicJob* = 0) { return false; }
	/*        */ inline bool DeleteItemInInventory(int i, INT64 q, class AtomicJob *job = 0) { return DeleteItemInInventory_0x0890(i, q, job); }
	/* 0x0898 */ virtual bool DeleteItemInInventory_0x0898(int) { return false; }
	/*        */ inline bool DeleteItemInInventory(int id) { return DeleteItemInInventory_0x0898(id); }
	/* 0x08A0 */ virtual bool DeleteItemInInventoryBeforeCommit_0x08A0(int, INT64) {}
	/* 0x08A8 */ virtual void LeaveWorld_0x08A8(bool) {}
	/* 0x08B0 */ virtual void MoveToLocation_0x08B0(int x, int y, int z, PathFindType pathFindType) {}
	/* 0x08B8 */ virtual void MoveToLocationRelative_0x08B8(class CAbstractCarrier *carrier, int x, int y, int z) {}
	/* 0x08C0 */ virtual bool TeleportToLocation_0x08C0(int, int, int, unsigned int, int, int, CCreature*) { return false; }
	/* 0x08C8 */ virtual bool TeleportWithAirship_0x08C8(class CAirShip *airship, FVector location, unsigned int) { return false; }
	/* 0x08D0 */ virtual bool GetOnAirShip_0x08D0(class CAirShip *airship) { return false; }
	/* 0x08D8 */ virtual bool GetOffAirShip_0x08D8(class CAirShip *airship, FVector location) { return false; }
	/* 0x08E0 */ virtual bool Die_0x08E0(CCreature *killer) { return false; }
	/* 0x08E8 */ virtual void Suicide_0x08E8(DamageTypeEnum) {}
	/* 0x08F0 */ virtual void SendResurrectionConfirmDlg_0x08F0() {}
	/* 0x08F8 */ virtual void vfn0x08F8() {}
	/* 0x0900 */ virtual void SocialAction_0x0900(int, int, bool) {}
	/* 0x0908 */ virtual void ChangeMoveType_0x0908(MoveType moveType) {}
	/* 0x0910 */ virtual double HeightDamage_0x0910(double d) { return 0.0; }
	/* 0x0918 */ virtual double GotDamage_0x0918(CCreature*, double, double, bool, DamageTypeEnum, bool) { return 0.0; }
	/* 0x0920 */ virtual double GotDamageBy_0x0920(CCreature*, double, bool, DamageTypeEnum, bool) { return 0.0; }
	/* 0x0928 */ virtual double AttackedBy_0x0928(CCreature*, double, bool, int, AttackType) { return 0.0; }
	/* 0x0930 */ virtual double GotDamageByEnvironment_0x0930(double, bool, DamageTypeEnum, bool) { return 0.0; }
	/* 0x0938 */ virtual void vfn0x0938() {}
	/* 0x0940 */ virtual void vfn0x0940() {}
	/* 0x0948 */ virtual void vfn0x0948() {}
	/* 0x0950 */ virtual void ChangeTalkTarget_0x0950(CObject*) {}
	/* 0x0958 */ virtual void Action_0x0958(CObject*, bool, bool) {}
	/* 0x0960 */ virtual bool CoupleAction_0x0960(int, bool, bool, int) { return false; }
	/* 0x0968 */ virtual bool SkillAction_0x0968(int, bool, bool, CItem*, const class CSkillInfo*) { return false; }
	/* 0x0970 */ virtual void OnUseSkillFinished_0x0970(int, int, int, int) {}
	/* 0x0978 */ virtual void vfn0x0978() {}
	/* 0x0980 */ virtual void ChangeTarget_0x0980(CObject*, ChangeTargetReason reason) {}
	/*        */ inline void ChangeTarget(CObject *object, ChangeTargetReason reason) { ChangeTarget_0x0980(object, reason); }
	/* 0x0988 */ virtual bool HaveSkill_0x0988(int, int) { return false; }
	/* 0x0990 */ virtual int GetSkillLevel_0x0990(int) {}
	/* 0x0998 */ virtual bool SaveSkillUsedTime_0x0998() { return false; }
	/* 0x09A0 */ virtual void vfn0x09A0() {}
	/* 0x09A8 */ virtual void ValidateSkillList_0x09A8() {}
	/* 0x09B0 */ virtual bool FreezeIfNobodyNearMe_0x09B0() { return false; }
	/* 0x09B8 */ virtual void vfn0x09B8() {}
	/* 0x09C0 */ virtual void vfn0x09C0() {}
	/* 0x09C8 */ virtual void SendStatusUpdate_0x09C8(CHAR_PARAM_TYPE charParamType) {}
	/* 0x09D0 */ virtual void vfn0x09D0() {}
	/* 0x09D8 */ virtual void vfn0x09D8() {}
	/* 0x09E0 */ virtual void vfn0x09E0() {}
	/* 0x09E8 */ virtual void vfn0x09E8() {}
	/* 0x09F0 */ virtual void vfn0x09F0() {}
	/* 0x09F8 */ virtual void vfn0x09F8() {}
	/* 0x0A00 */ virtual void vfn0x0A00() {}
	/* 0x0A08 */ virtual void vfn0x0A08() {}
	/* 0x0A10 */ virtual void vfn0x0A10() {}
	/* 0x0A18 */ virtual void vfn0x0A18() {}
	/* 0x0A20 */ virtual void vfn0x0A20() {}
	/* 0x0A28 */ virtual void vfn0x0A28() {}
	/* 0x0A30 */ virtual void vfn0x0A30() {}
	/* 0x0A38 */ virtual void vfn0x0A38() {}
	/* 0x0A40 */ virtual void vfn0x0A40() {}
	/* 0x0A48 */ virtual void vfn0x0A48() {}
	/* 0x0A50 */ virtual void vfn0x0A50() {}
	/* 0x0A58 */ virtual void vfn0x0A58() {}
	/* 0x0A60 */ virtual void vfn0x0A60() {}
	/* 0x0A68 */ virtual void vfn0x0A68() {}
	/* 0x0A70 */ virtual void vfn0x0A70() {}
	/* 0x0A78 */ virtual void vfn0x0A78() {}
	/* 0x0A80 */ virtual void vfn0x0A80() {}
	/* 0x0A88 */ virtual bool OnMagicSkillUsePacket_0x0A88(int, bool, bool) { return false; }
	/* 0x0A90 */ virtual bool IsInDuel_0x0A90() { return false; }
	/* 0x0A98 */ virtual bool IsDuelEnemy_0x0A98(CCreature*) { return false; }
	/* 0x0AA0 */ virtual bool IsDuelAlly_0x0AA0(CCreature*) { return false; }
	/* 0x0AA8 */ virtual void vfn0x0AA8() {}
	/* 0x0AB0 */ virtual void vfn0x0AB0() {}
	/* 0x0AB8 */ virtual void vfn0x0AB8() {}
	/* 0x0AC0 */ virtual void vfn0x0AC0() {}
	/* 0x0AC8 */ virtual class CCreatureController* GetCreatureController_0x0AC8() { return 0; }
	/*        */ inline class CCreatureController* GetCreatureController() { return GetCreatureController_0x0AC8(); }
	/* 0x0AD0 */ virtual void vfn0x0AD0() {}
	/* 0x0AD8 */ virtual void _OnAbnormalStatusChanged_0x0AD8(const class CSkillInfo&, CCreature*, SkillPumpStartType pumpStartType, bool) {}
	/* 0x0AE0 */ virtual void _OnAbnormalStatusDeleted_0x0AE0(const class CSkillInfo&, CCreature*, SkillPumpEndType pumpEndType, bool) {}
	/* 0x0AE8 */ virtual void OnCorpseDecayed_0x0AE8() {}
	/* 0x0AF0 */ virtual bool OnRegenTimer_0x0AF0(FVector &location) { return false; }
	/* 0x0AF8 */ virtual void OnActivate_0x0AF8() {}
	/* 0x0B00 */ virtual void vfn0x0B00() {}
	/* 0x0B08 */ virtual void vfn0x0B08() {}
	/* 0x0B10 */ virtual void vfn0x0B10() {}
	/* 0x0B18 */ virtual void vfn0x0B18() {}
	/* 0x0B20 */ virtual void vfn0x0B20() {}
	/* 0x0B28 */ virtual CUserSocket* GetUserSocket_0x0B28() { return 0; }
	/*        */ inline CUserSocket* GetUserSocket() { return GetUserSocket_0x0B28(); }
	/* 0x0B30 */ virtual void SetNickName_0x0B30(const wchar_t *name) {}
	/*        */ inline void SetNickName(const wchar_t *name) { SetNickName_0x0B30(name); }
	/* 0x0B38 */ virtual void vfn0x0B38() {}

	static void Init();

	void SendSystemMessageFmt(const wchar_t *sender, const wchar_t *format, ...);
	bool AddItemToInventory(int itemType, INT64 amount, ItemGetReason reason, int enchant, int unknown1, int unknown2, const ItemOptionKey &option, int unknown3, const BaseAttribute &attribute);
	bool AddItemToInventory(int itemType, INT64 amount, int enchant, const ItemOptionKey &option, const BaseAttribute &attribute);
	bool AddItemToInventory(int itemType, INT64 amount, int enchant = 0);
	bool AddItemToInventoryAutoloot(int itemType, INT64 amount, ItemGetReason reason, int enchant, int unknown1, int unknown2, const ItemOptionKey &option, int unknown3, const BaseAttribute &attribute);
	CSPointer<CObject> GetTarget();
	void OutOfSightWrapper(CObject *object, bool b);
	void DoNothing();
	unsigned long GetSkillUsedTime(const int skillId);
	int GetRemainReuseDelaySec(const int skillId);
	void GetBonusBoundary(int difference, double *bonus1, double *bonus2);
	bool OnMagicSkillUseIsOnAirship();
	void GatherNeighborCreature(FVector position, int type, int limit, bool unknown,
	                            xstd::vector<CSafePointer2<CCreature> > &targets,
	                            xstd::vector<double> &distances,
	                            int range, int heightMin, int heightMax,
	                            SkillAffectObjectTypeEnum affectObjectType,
	                            CCreature *caster,
	                            CCreature *excluded,
	                            bool unknown2 = false,
	                            bool unknown3 = false);
	/* 0x0058 */ unsigned char padding0x0058[0x0A88 - 0x0058];
	/* 0x0A88 */ CSharedCreatureData *sd;
	/* 0x0A90 */ unsigned char padding0x0A90[0x0AA0 - 0x0A90];
	/* 0x0AA0 */ CInventory inventory;
	/* 0x0B70 */ unsigned char padding0x0B70[0x15D0 - 0x0B70];
	/* 0x15D0 */ INT32 targetId;
	/* 0x15D4 */ unsigned char padding0x15D4[0x15D8 - 0x15D4];
	/* 0x15D8 */ CCreatureController *creatureController;
	/* 0x15E0 */ unsigned char padding0x15E0[0x1CA0 - 0x15E0];
	/* 0x1CA0 */ CContributeData *contributeData;
	/* 0x1CA8 */ unsigned char padding0x1CA8[0x1E61 - 0x1CA8];
	/* 0x1E61 */ bool leftWorld;
	/* 0x1E62 */ unsigned char padding0x1E62[0x1F28 - 0x1E62];
	/* 0x1F28 */

};

} // namespace l2server

