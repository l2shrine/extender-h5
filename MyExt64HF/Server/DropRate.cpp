
#include <Server/DropRate.h>
#include <Server/CNPC.h>
#include <Common/Config.h>
#include <Common/CSharedCreatureData.h>
#include <Common/CLog.h>
#include <Common/Utils.h>
#include <algorithm>
#include <cmath>
#include <fstream>

namespace l2server {

void DropRate::Init()
{
	if (Config::Instance()->rate->dump) {
		std::ofstream("..\\dump\\dropspoil.txt").close();
	}
}

void DropRate::Apply(const CNPC *npc, const enum ObjectFieldType objectFieldType, xstd::vector<ItemDropMultiStruct*> *itemDrop)
{
	GUARDED;

	double dropRate;
	if (objectFieldType == ObjectFieldTypeExItemDropList) {
		dropRate = Config::Instance()->rate->herbRate;
	} else {
		dropRate = npc->IsBoss() ? Config::Instance()->rate->bossDropRate : Config::Instance()->rate->dropRate;
		if (Config::Instance()->rate->fixupLowLevel && npc->sd->level < 20) {
			dropRate = std::max(1.0, floor(0.5 + std::pow(npc->sd->level / 20.0, std::log(dropRate) / std::log(4.0)) * dropRate));
		}
	}

	int groupId = 0;
	for (xstd::vector<ItemDropMultiStruct*>::iterator i(itemDrop->begin()) ; i != itemDrop->end() ; ++i) {
		bool stopGroup(false);
		double oldGroupChance = (*i)->chance;
		size_t itemCount = 0;
		std::vector<ItemDropStruct> oldItems;

		for (xstd::vector<ItemDropStruct*>::iterator ii((*i)->items->begin()) ;
			ii != (*i)->items->end() ;
			++ii) {

			ItemDropStruct drop;
			drop.amountMin = (*ii)->amountMin;
			drop.amountMax = (*ii)->amountMax;
			drop.chance = (*ii)->chance;
			oldItems.push_back(drop);

			if ((*ii)->itemType == 57) {
				(*ii)->amountMin = int(std::max(1.0, double((*ii)->amountMin) * Config::Instance()->rate->adenaRate));
				(*ii)->amountMax = int(std::max(1.0, double((*ii)->amountMax) * Config::Instance()->rate->adenaRate));
				stopGroup = true;
			} else if (Config::Instance()->rate->ignoredItems.count((*ii)->itemType)) {
				stopGroup = true;
			}

			if ((*ii)->itemType >= 6360 && (*ii)->itemType <= 6362) {
				(*ii)->amountMin *= Config::Instance()->rate->sealStoneAmountRate;
				(*ii)->amountMax *= Config::Instance()->rate->sealStoneAmountRate;
			}

			++itemCount;
		}

		if (!stopGroup) {
			(*i)->chance *= dropRate;
			if ((*i)->chance > 100.0) {
				(*i)->chance = 100.0;
				double adjustedDropRate = dropRate * oldGroupChance * 0.01;
				double threshold = 100.0 / adjustedDropRate / double(itemCount);
				double s1 = 0.0, s2 = 0.0;

				for (xstd::vector<ItemDropStruct*>::iterator ii((*i)->items->begin()) ;
					ii != (*i)->items->end() ;
					++ii) {

					bool overThreshold = (*ii)->chance > threshold;
					double exp = 1.0;
					if (overThreshold) {
						double delta1 = (*ii)->chance - threshold;
						double delta2 = 100.0 - threshold;
						exp = -delta1 / delta2;
					}
					(*ii)->chance *= pow(adjustedDropRate, exp);
					if (overThreshold) {
						s1 += (*ii)->chance;
					} else {
						s2 += (*ii)->chance;
					}
				}

				std::vector<ItemDropStruct>::iterator ioldItems(oldItems.begin());
				for (xstd::vector<ItemDropStruct*>::iterator ii((*i)->items->begin()) ;
					ii != (*i)->items->end() ;
					++ii, ++ioldItems) {

					if (ioldItems->chance > threshold) {
						(*ii)->chance *= (100.0 - s2) / s1;
					}
					double coef = ioldItems->amountMin + ioldItems->amountMax;
					coef *= ioldItems->chance * oldGroupChance;
					double denom = (*ii)->amountMin + (*ii)->amountMax;
					denom *= (*ii)->chance * (*i)->chance;
					coef /= denom;
					coef *= dropRate;
					(*ii)->amountMin = int(std::max(1.0, std::floor(0.5 + (*ii)->amountMin * std::max(1.0, coef))));
					(*ii)->amountMax = int(std::max(1.0, std::floor(0.5 + (*ii)->amountMax * std::max(1.0, coef))));
				}
			}
		}

		if (Config::Instance()->rate->dump) {
			for (xstd::vector<ItemDropStruct*>::iterator ii((*i)->items->begin()) ;
				ii != (*i)->items->end() ;
				++ii) {

				std::ofstream("..\\dump\\dropspoil.txt", std::ios::app)
					<< (npc->objectType - 1000000) << "\t"
					<< static_cast<int>(objectFieldType) << "\t"
					<< (*ii)->itemType << "\t"
					<< (*ii)->amountMin << "\t"
					<< (*ii)->amountMax << "\t"
					<< (*ii)->chance * (*i)->chance * 0.01 << "\t"
					<< groupId << "\t"
					<< (*i)->chance << std::endl;
			}
		}

		++groupId;
	}
}

void DropRate::Apply(const CNPC *npc, const enum ObjectFieldType objectFieldType, xstd::vector<ItemDropStruct*> *itemDrop)
{
	GUARDED;

	double dropRate = objectFieldType == ObjectFieldTypeCorpseMakeList
		? Config::Instance()->rate->spoilRate
		: npc->IsBoss()
			? Config::Instance()->rate->bossDropRate
			: Config::Instance()->rate->dropRate;

	if (Config::Instance()->rate->fixupLowLevel && npc->sd->level < 20) {
		dropRate = std::max(1.0, std::floor(0.5 + std::pow(npc->sd->level / 20.0, std::log(dropRate) / std::log(4.0)) * dropRate));
	}

	for (xstd::vector<ItemDropStruct*>::iterator i(itemDrop->begin()) ; i != itemDrop->end() ; ++i) {
		ItemDropStruct &item(**i);
		ItemDropStruct old(item);
		if (item.itemType == 57) {
			item.amountMin = int(std::max(1.0, double(item.amountMin) * Config::Instance()->rate->adenaRate));
			item.amountMax = int(std::max(1.0, double(item.amountMax) * Config::Instance()->rate->adenaRate));
		} else if (!Config::Instance()->rate->ignoredItems.count(item.itemType)) {
			double score = double(item.amountMin + item.amountMax) * item.chance;
			item.chance *= dropRate;
			if (item.chance > 100.0) {
				score *= dropRate;

				item.amountMin = int(std::max(1.0, std::floor(0.5 + item.amountMin * dropRate / 100.0)));
				item.amountMax = int(std::max(1.0, std::floor(0.5 + item.amountMax * dropRate / 100.0)));

				for (;;) {
					item.chance = score / double(item.amountMin + item.amountMax);
					if (item.chance <= 100.0) {
						break;
					}
					++item.amountMax;
				}
			}
		}

		if (item.itemType >= 6360 && item.itemType <= 6362) {
			item.amountMin *= Config::Instance()->rate->sealStoneAmountRate;
			item.amountMax *= Config::Instance()->rate->sealStoneAmountRate;
		}

		if (Config::Instance()->rate->dump) {
			std::ofstream("..\\dump\\dropspoil.txt", std::ios::app)
				<< (npc->objectType - 1000000) << "\t"
				<< static_cast<int>(objectFieldType) << "\t"
				<< item.itemType << "\t"
				<< item.amountMin << "\t"
				<< item.amountMax << "\t"
				<< item.chance << "\t"
				<< "-1" << "\t"
				<< 100.0 << std::endl;
		}
	}
}

} // namespace l2server
