
#include <Server/CSummon.h>
#include <Server/CPet.h>
#include <Server/User.h>
#include <Server/CParty.h>
#include <Common/Config.h>

namespace l2server {

void CSummon::Init()
{
	// Friendly command channel
	WriteMemoryQWORD(CSummon::vtable + 0x410, FnPtr(&CSummon::IsEnemyToWrapper));
	WriteMemoryQWORD(CPet::vtable + 0x410, FnPtr(&CSummon::IsEnemyToWrapper));
}

bool CSummon::IsEnemyToWrapper(CCreature *creature)
{
	bool result = reinterpret_cast<bool(*)(CSummon*, CCreature*)>(0x82CBE4)(this, creature);
	if (!Config::Instance()->fixes->commandChannelFriendly) return result;
	User *owner = GetUserOrMaster();
	if (!owner) return result;
	CParty *party = owner->GetParty();
	CMultiPartyCommandChannel *mpcc = owner->GetMPCC();
	User *user = creature->GetUserOrMaster();
	if (user) {
		if (user == owner) return false;
		if (party && user->GetParty() == party) return false;
		if (mpcc && user->GetMPCC() == mpcc) return false;
	}
	return result;
}

} // namespace l2server

