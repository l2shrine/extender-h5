
#include <Server/CWorld.h>
#include <Server/CItem.h>
#include <Server/User.h>
#include <Server/CUserSocket.h>
#include <Server/CSummon.h>
#include <Server/CNPC.h>
#include <Server/Server.h>
#include <Server/CObjectDB.h>
#include <Server/CContributeData.h>
#include <Server/CCursedWeaponMgr.h>
#include <Common/Utils.h>
#include <Common/Config.h>
#include <Common/CSharedCreatureData.h>
#include <Common/CSharedItemData.h>
#include <Common/CLog.h>
#include <algorithm>
#include <map>
#include <vector>

namespace l2server {

void CWorld::Init()
{
	// Autoloot
	WriteInstructionCall(0x79C4E5, FnPtr(&CWorld::PutItemNPCDrop));

	WriteMemoryBYTES(0xA459B8, "\x48\x89\xE9", 3); // mov rcx, rbp (CItem*)
	WriteInstructionCallJmpEax(0xA459B8 + 3, FnPtr(PickItemIsInWorldHelper));

	const char *PickItemIsInWorldHelper2Caller =
		/* 0x00 */ "\x49\x8D\x4D\xA8" // lea rcx, [r13-0x58]
		/* 0x04 */ "\xE8\x00\x00\x00\x00" // placeholder for call
		/* 0x09 */ "\x4D\x8B\x5D\x00" // mov r11, [r13+0]
		/* 0x0D */ "\xFF\xE0" // jmp rax
		/* 0x0F */ "PickItemIsInWorldHelper2Caller"; // to make sure it's not equal to any other code piece
	MakeExecutable(reinterpret_cast<UINT32>(PickItemIsInWorldHelper2Caller), 0x0F);
	WriteInstructionCall(reinterpret_cast<UINT32>(PickItemIsInWorldHelper2Caller) + 4, FnPtr(PickItemIsInWorldHelper2));
	WriteInstructionJmp(0xA433FE, reinterpret_cast<UINT32>(PickItemIsInWorldHelper2Caller));

	WriteMemoryBYTES(0xA3EF3C, "\x49\x89\xC1\x90", 4); // mov r9, rax ; nop
	WriteInstructionCall(0xA3EF51, FnPtr(AsyncPickLogHelper));

	WriteMemoryBYTES(0xA3EB86, "\x49\x89\xC1\x90", 4); // mov r9, rax ; nop
	WriteInstructionCall(0xA3EB9D, FnPtr(AsyncPickLogHelper));

	const char *PickEffectHelperCaller =
		/* 0x00 */ "\x48\x89\xDA" // mov rdx, rbx
		/* 0x03 */ "\xE8\x00\x00\x00\x00" // placeholder for call
		/* 0x08 */ "\xFF\xE0" // jmp rax
		/* 0x0A */ "PickEffectHelper"; // to make sure it's not equal to any other code piece
	MakeExecutable(reinterpret_cast<UINT32>(PickEffectHelperCaller), 0x0A);
	WriteInstructionCall(reinterpret_cast<UINT32>(PickEffectHelperCaller) + 3, FnPtr(PickEffectHelper));
	WriteInstructionJmp(0xA43F01, reinterpret_cast<UINT32>(PickEffectHelperCaller));
}

CWorld* CWorld::Instance()
{
	return reinterpret_cast<CWorld*(*)()>(0xA34540)();
}

template<class T>
class second_less {
public:
	bool operator()(const T &v1, const T &v2)
	{
		return v1.second < v2.second;
	}
};

bool CWorld::PutItemNPCDrop(CItem *item, FVector &pos, CNPC *npc)
{
	GUARDED;

	if (!item) return false;
	if (!npc) return PutItem(item, pos, npc);
	if (item->itemInfo->immediateEffect || item->itemInfo->forceEquip) return PutItem(item, pos, npc);
	if (CCursedWeaponMgr::Instance()->constInfo.Find(item->objectType)) return PutItem(item, pos, npc);
	if (Config::Instance()->autoLoot->excludedItems.count(item->objectType)) return PutItem(item, pos, npc);
	if (npc->IsBoss() && !Config::Instance()->autoLoot->autoLootBossDrop) return PutItem(item, pos, npc);
	if (!npc->IsBoss() && !Config::Instance()->autoLoot->autoLootMobDrop) return PutItem(item, pos, npc);

	CContributeData *contributeData = item->GetContributeData();
	if (!contributeData) return PutItem(item, pos, npc);

	std::vector<std::pair<User*, double> > contributors;
	double maxd = Config::Instance()->autoLoot->maximumAutoLootDistance;

	for (xstd::map<int, double>::const_iterator i = contributeData->data.begin() ; i != contributeData->data.end() ; ++i) {

		CCreature *creature = reinterpret_cast<CCreature*>(CObjectDB::GetObject(i->first));

		if (!creature) {
			continue;
		}

		if (!creature->IsUser()) {
			if (!creature->IsSummonOrPet()) {
				continue;
			}

			creature = reinterpret_cast<CSummon*>(creature)->GetUserOrMaster();

			if (!creature || !creature->IsUser()) {
				continue;
			}
		}

		User *contributor = reinterpret_cast<User*>(creature);

		if (!contributor->IsAlive()
			|| !contributor->GetUserSocket()
			|| !contributeData->PickableIn5Sec(contributor)) {

			continue;
		}

		if (!contributor->sd->location.IsInRange(pos, maxd)) continue;

		contributors.push_back(std::make_pair(contributor, i->second));
	}

	if (contributors.empty()) return PutItem(item, pos, npc);

	std::sort(contributors.begin(), contributors.end(), second_less<std::pair<User*, double> >());

	User *looter = contributors.back().first;

	CUserSocket *socket = looter->GetUserSocket();
	if (!socket) return PutItem(item, pos, npc);

	item->sd->location = looter->sd->location;
	item->ext.autolooted = true;
	item->ext.autolootedAsync = true;
	if (!PickItem(item, looter)) {
		item->ext.autolooted = false;
		item->ext.autolootedAsync = false;
		return PutItem(item, pos, npc);
	}
	item->contributeDataObjectId = 0;
	item->deleteOnTimer = false;

	return true;
}

bool CWorld::PutItem(CItem *item, FVector &pos, CNPC *npc)
{
	if (!item || !npc) return false;
	return reinterpret_cast<bool(*)(CWorld*, CItem*, FVector&, CNPC*)>(0xA47B9C)(
		this, item, pos, npc);
}

bool CWorld::PickItem(CItem *item, User *user)
{
	if (!item || !user) return false;
	return reinterpret_cast<bool(*)(CWorld*, CItem*, User*)>(0xA4322C)(
		this, item, user);
}

UINT64 CWorld::PickItemIsInWorldHelper(CItem *item)
{
	return (item->sd->inWorld || item->ext.autolooted) ? 0xA459D6 : 0xA459C2;
}

UINT64 CWorld::PickItemIsInWorldHelper2(CItem *item)
{
	return (item->sd->inWorld || item->ext.autolooted) ? 0xA4342B : 0xA43408;
}

void CWorld::AsyncPickLogHelper(void *logInstance, int type, const char *format, CItem *item, const char *file, int line)
{
	if (item->ext.autolootedAsync) {
		item->ext.autolootedAsync = false;
		return;
	}
	reinterpret_cast<void(*)(void*, int, const char*, int, const char*, int)>(0x73446C)(logInstance, type, format, item->sd->containerIndex, file, line);
}

UINT64 CWorld::PickEffectHelper(CCreature *creature, CItem *item)
{
	if (item->ext.autolooted) {
		item->ext.autolooted = false;
		return 0xA44004;
	}
	if (creature->IsInvisible()) return 0xA43F0C;
	return 0xA43F86;
}

} // namespace l2server

