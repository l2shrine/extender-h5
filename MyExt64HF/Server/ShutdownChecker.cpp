
#include <Server/ShutdownChecker.h>
#include <Common/Utils.h>
#include <fstream>
#include <string>

namespace l2server {

void ShutdownChecker::Init()
{
	std::ofstream ofs("status.txt");
	if (!ofs) return;
	ofs << "starting" << std::endl;
	ofs.close();
}

void ShutdownChecker::Start()
{
	GUARDED;

	std::ofstream ofs("status.txt");
	if (!ofs) return;
	ofs << "running" << std::endl;
	ofs.close();
}

void ShutdownChecker::CheckForShutdown()
{
	GUARDED;

	std::ifstream ifs("status.txt");
	if (!ifs) return;
	std::string status;
	ifs >> status;
	ifs.close();
	if (status != "shutdown") return;
	reinterpret_cast<void(*)(HWND, UINT, WPARAM, LPARAM)>(0x72D880)(
		GetActiveWindow(), 0x111, 0x97, 0x0);
	std::ofstream ofs("status.txt");
	if (!ofs) return;
	ofs << "shutdown_in_progress" << std::endl;
	ofs.close();
}

} // namespace server

