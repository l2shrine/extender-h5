
#pragma once

#include <Server/CSocket.h>
#include <string>
#include <Windows.h>

namespace l2server {

class User;

class CUserSocket : public CSocket {
public:
	class Ext {
	public:
		Ext();
		~Ext();

		bool offlineTradeEnded;
	};

	// Static initialization
	static void Init();

	// Constructor
	CUserSocket* Constructor(UINT64 handle);

	// Destructor
	void* Destructor(bool freeMemory);

	// Forcibly close socket
	void ForceClose(int reason = 6);

	// Wrapper for _LoginPacket function
	bool _LoginPacket(const wchar_t *accountName, const int accountId, const int unknown1, const int unknown2, const int sessionId, const int unknown3, const int unknown4);

	// Returns client IP address as a string
	std::wstring GetIP() const;

	// Processes voice command, returns true if it was legal voice command
	bool VoiceCommand(const std::wstring &command);

	// Wrapper for SayPacket2 function
	bool SayPacket2(const unsigned char *packet);

	// Set close timer
	void SetCloseTimer();

	// Adds timer unless offline trade is being closed right now
	void AddTimerOfflineTrade(const int interval, const int id);

	static bool PrivateStoreListSet(CUserSocket *socket, const unsigned char *data);
	static bool PrivateStoreBuyListSet(CUserSocket *socket, const unsigned char *data);
	static bool RequestRecipeShopSetList(CUserSocket *socket, const unsigned char *data);

	/* 0x00C0 */ virtual class User* GetUser() { return 0; }
	/* 0x00C8 */ virtual int GetSocketUID() { return 0; }

	/* 0x00D8 */ unsigned char padding0x00D8[0x04F0 - 0x00D8];
	/* 0x04F0 */ class User *user;
	/* 0x04F8 */ unsigned char padding0x04F8[0x0534 - 0x04F8];
	/* 0x0534 */ bool unknownFlag0;
	/* 0x0535 */ bool unknownFlag1;
	/* 0x0536 */ bool unknownFlag2;
	/* 0x0537 */ bool unknownFlag3;
	/* 0x0538 */ bool isPremiumUser;
	/* 0x0539 */ unsigned char padding0x0539[0x0CDC - 0x0539];

	// this is already after original CUserSocket but we need ext to be aligned to QWORD boundary:
	/* 0x0CDC */ unsigned char padding0x0CDC[0x0CE0 - 0x0CDC];
	/* 0x0CE0 */ Ext ext;
};

} // namespace l2server

