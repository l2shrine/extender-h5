
#include <Server/NpcServer.h>
#include <Server/NpcSocket.h>
#include <Server/User.h>
#include <Server/CObjectDB.h>
#include <Server/CContributeData.h>
#include <Server/CDB.h>
#include <Server/CUserBroadcaster.h>
#include <Common/Utils.h>
#include <Common/CLog.h>
#include <Common/CSharedCreatureData.h>
#include <Common/CSPointer.h>
#include <Common/Enum.h>
#include <set>
#include <time.h>

namespace l2server {

void NpcServer::Init()
{
	WriteAddress(0xB0AC07, FnPtr(&NpcServer::NpcEx));
}

NpcServer* NpcServer::Instance()
{
	return reinterpret_cast<NpcServer*>(0x3047E40);
}

NpcSocket* NpcServer::GetNpcSocket()
{
	return reinterpret_cast<NpcSocket*(*)(NpcServer*)>(0x7ACC00)(this);
}

void NpcServer::Send(const char *format, ...)
{
	GUARDED;
	NpcSocket *socket = GetNpcSocket();
	if (!socket) return;

	va_list va;
	va_start(va, format);
	socket->SendV(format, va);
	va_end(va);
}

bool NpcServer::NpcEx(NpcSocket *socket, const unsigned char *packet)
{
	UINT16 opcode = 0;
	const unsigned char *data = Disassemble(packet, "h", &opcode);
	switch (opcode) {
	case NpcExtPacket::RIDE_WYVERN2: return NpcRideWyvern2(socket, data);
	case NpcExtPacket::CLEAR_CONTRIBUTE_DATA: return ClearContributeData(socket, data);
	case NpcExtPacket::SET_PREMIUM: return SetPremium(socket, data);
	case NpcExtPacket::BROADCAST_ANNOUNCE: return BroadcastAnnounce(socket, data);
	case NpcExtPacket::BROADCAST_RED_SKY: return BroadcastRedSky(socket, data);
	}
	return true;
}

bool NpcServer::NpcRideWyvern2(NpcSocket *socket, const unsigned char *packet)
{
	UINT32 talkerIndex;
	UINT32 classId;
	UINT32 duration;
	Disassemble(packet, "ddd", &talkerIndex, &classId, &duration);
	CSPointer<CCreature> talker = CObjectDB::GetObjectByIndex<CCreature>(talkerIndex);
	if (!talker) return false;
	if (!talker->IsUser()) return false;
	User *user = talker->CastUser();
	if (user->sd->stopMode != 1) return false;
	if (user->IsNowTrade()) return false;
	if (!user->IsItemUsable()) return false;
	if (user->IsRiding()) return false;
	user->RideForEvent(classId, duration, false);
	return false;
}

bool NpcServer::ClearContributeData(NpcSocket *socket, const unsigned char *packet)
{
	UINT32 npcIndex;
	Disassemble(packet, "d", &npcIndex);
	CSPointer<CCreature> npc = CObjectDB::GetObjectByIndex<CCreature>(npcIndex);
	if (npc) {
		npc->contributeData->Clear();
	}
	return false;
}

bool NpcServer::SetPremium(NpcSocket *socket, const unsigned char *packet)
{
	int index;
	UINT32 seconds;
	Disassemble(packet, "dd", &index, &seconds);
	CSPointer<CObject> object = CObjectDB::GetObjectByIndex<CObject>(index);
	if (!object || !object->IsUser()) return false;
	User *user(object->CastUser());
	CDB::Instance()->RequestSetPremium(user->GetUserAccountId(), seconds + time(0));
	return false;
}

bool NpcServer::BroadcastAnnounce(NpcSocket *socket, const unsigned char *packet)
{
	wchar_t message[1024];
	Disassemble(packet, "S", sizeof(message), message);
	CUserBroadcaster::Instance()->BroadcastToAllUser("cddSdS", 0x4A, 0, 0x12, "", -1, message);
	return false;
}

bool NpcServer::BroadcastRedSky(NpcSocket *socket, const unsigned char *packet)
{
	UINT32 duration;
	Disassemble(packet, "d", &duration);
	CUserBroadcaster::Instance()->BroadcastToAllUser("chd", 0xFE, 0x41, duration);
	return false;
}

} // namespace l2server

