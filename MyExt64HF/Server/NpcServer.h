
#pragma once

#include <stdarg.h>
#include <windows.h>

namespace l2server {

class NpcSocket;

class NpcServer {
public:
	static void Init();
	static NpcServer* Instance();
	NpcSocket* GetNpcSocket();
	void Send(const char *format, ...);
	static bool NpcEx(NpcSocket *socket, const unsigned char *packet);
	static bool NpcRideWyvern2(NpcSocket *socket, const unsigned char *packet);
	static bool ClearContributeData(NpcSocket *socket, const unsigned char *packet);
	static bool SetPremium(NpcSocket *socket, const unsigned char *packet);
	static bool BroadcastAnnounce(NpcSocket *socket, const unsigned char *packet);
	static bool BroadcastRedSky(NpcSocket *socket, const unsigned char *packet);

	/* 0x0000 */ unsigned char padding0x0000[0x00A8 - 0x0000];
	/* 0x00A8 */ NpcSocket *socket;
};

} // namespace l2server

