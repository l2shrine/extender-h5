
#pragma once

#include <Server/CWorldObject.h>
#include <Common/Enum.h>

class CYieldLock;
class CSharedItemData;

namespace l2server {

class User;
class CContributeData;

class ItemOptionKey {
public:
	ItemOptionKey();

	int option1;
	int option2;
};

class BaseAttribute {
public:
	BaseAttribute();

	INT16 type;
	INT16 value;
	INT16 defenseFire;
	INT16 defenseWater;
	INT16 defenseWind;
	INT16 defenseEarth;
	INT16 defenseHoly;
	INT16 defenseUnholy;
	INT16 unknown;
};

class CItem : public CWorldObject {
public:
	class ItemInfo {
	public:
		/* 0x0000 */ unsigned char padding0x0000[0x0075 - 0x0000];
		/* 0x0075 */ bool immediateEffect;
		/* 0x0076 */ unsigned char padding0x0076[0x0136 - 0x0076];
		/* 0x0136 */ bool forceEquip;
		/* 0x0137 */ unsigned char padding0x0137[0x0158 - 0x0137];
		/* 0x0158 */ int crystalType;
		/* 0x015C */ unsigned char padding0x015C[0x01A0 - 0x015C];
	};

	class Ext {
	public:
		Ext();
		~Ext();

		bool autolooted;
		bool autolootedAsync;
	};

	static void Init();

	CItem* Constructor(bool unknown);
	CItem* CopyConstructor(const CItem &other);
	void* Destructor(bool freeMemory);

	/* 0x0060 */ virtual void Delete() {}

	CContributeData* GetContributeData();

	/* 0x0058 */ CSharedItemData *sd;
	/* 0x0060 */ ItemInfo *itemInfo;
	/* 0x0068 */ CYieldLock *lock;
	/* 0x0070 */ unsigned char padding0x0070[0x0080 - 0x0070];
	/* 0x0080 */ bool deleteOnTimer;
	/* 0x0081 */ unsigned char padding0x0081[0x0084 - 0x0081];
	/* 0x0084 */ unsigned int contributeDataObjectId;
	/* 0x0088 */ unsigned char padding0x0088[0x0120 - 0x0088];
	/* 0x0120 */ BaseAttribute baseAttribute;
	/* 0x0132 */ unsigned char padding0x0132[0x0150 - 0x0132];
	/* 0x0150 */ unsigned char reserved0x0150[0x0158 - 0x0150];
	/* 0x0158 */ Ext ext;
};

} // namespace l2server

