
#include <Server/global.h>

namespace l2server {
namespace global {

RWLock *authSessionLock = reinterpret_cast<RWLock*>(0x12CB3C0);
xstd::map<int, int> *authAccountSessionMap = reinterpret_cast<xstd::map<int, int>*>(0x113AFE8);
int &fontcheck = *reinterpret_cast<int*>(0x1312C3C);

} // namespace global
} // namespace l2server

