
#include <Server/CDB.h>
#include <Server/User.h>
#include <Server/CObjectDB.h>
#include <Common/CLog.h>
#include <Common/Utils.h>
#include <Common/Enum.h>
#include <Common/CSharedCreatureData.h>
#include <Common/Config.h>
#include <Common/CSPointer.h>

namespace l2server {

void CDB::Init()
{
	// Fix race condition leading to loss of fame points
	WriteInstructionCall(0x4426AC + 0x11D, FnPtr(&CDB::SendSaveCharacterInfo));
	WriteInstructionCall(0x557724 + 0x143, FnPtr(&CDB::SendSaveCharacterInfo));
	WriteInstructionCall(0x59097C + 0x263, FnPtr(&CDB::SendSaveCharacterInfo));
	WriteInstructionCall(0x59A6D8 + 0x209, FnPtr(&CDB::SendSaveCharacterInfo));
	WriteInstructionCall(0x5B22E4 + 0x148, FnPtr(&CDB::SendSaveCharacterInfo));
	WriteInstructionCall(0x5B29D0 + 0x16C, FnPtr(&CDB::SendSaveCharacterInfo));
	WriteInstructionCall(0x7EB48C + 0x167, FnPtr(&CDB::SendSaveCharacterInfo));
	WriteInstructionCall(0x7F4080 + 0x140, FnPtr(&CDB::SendSaveCharacterInfo));
	WriteInstructionCall(0x8E6380 + 0xFC, FnPtr(&CDB::SendSaveCharacterInfo));
	WriteInstructionCall(0x8E64D8 + 0xFC, FnPtr(&CDB::SendSaveCharacterInfo));
	WriteInstructionCall(0x8E6630 + 0xFC, FnPtr(&CDB::SendSaveCharacterInfo));
	WriteInstructionCall(0x8E6BB0 + 0x119, FnPtr(&CDB::SendSaveCharacterInfo));
	WriteInstructionCall(0x93BFA8 + 0x13C, FnPtr(&CDB::SendSaveCharacterInfo));
	WriteInstructionCall(0x944FE4 + 0xC2, FnPtr(&CDB::SendSaveCharacterInfo));
	WriteInstructionCall(0x9539A4 + 0x170, FnPtr(&CDB::SendSaveCharacterInfo));
	WriteInstructionCall(0x953B6C + 0x161, FnPtr(&CDB::SendSaveCharacterInfo));
	WriteInstructionCall(0x962064 + 0x7CE, FnPtr(&CDB::SendSaveCharacterInfo));
	WriteInstructionCall(0x96ADD8 + 0x11DD, FnPtr(&CDB::SendSaveCharacterInfo));
	WriteInstructionCall(0x978B00 + 0x1EA3, FnPtr(&CDB::SendSaveCharacterInfo));
	WriteInstructionCall(0x978B00 + 0x21D4, FnPtr(&CDB::SendSaveCharacterInfo));
	WriteInstructionCall(0x978B00 + 0x22A9, FnPtr(&CDB::SendSaveCharacterInfo));
	WriteInstructionCall(0x978B00 + 0x2323, FnPtr(&CDB::SendSaveCharacterInfo));
	WriteInstructionCall(0x978B00 + 0x2404, FnPtr(&CDB::SendSaveCharacterInfo));
	WriteInstructionCall(0x993310 + 0x24F, FnPtr(&CDB::SendSaveCharacterInfo));
	WriteInstructionCall(0x996CBC + 0x705, FnPtr(&CDB::SendSaveCharacterInfo));
	WriteInstructionCall(0x9BAAC8 + 0x85, FnPtr(&CDB::SendSaveCharacterInfo));
	WriteInstructionCall(0x9D2B84 + 0x17A, FnPtr(&CDB::SendSaveCharacterInfo));

	// Packet handler
	WriteAddress(0xB03F9C + 3, FnPtr(ReplyExPacket));

	// Disable navit system
	WriteInstructionCall(0x996550, FnPtr(&CDB::RequestLoadNavitAdvent));
	WriteInstructionCall(0x4884BF, FnPtr(&CDB::RequestSaveNavitAdvent));
	WriteInstructionCall(0x488BD3, FnPtr(&CDB::RequestSaveNavitAdvent));
	WriteInstructionCall(0x9979D3, FnPtr(&CDB::RequestSaveNavitAdvent));
}

CDB* CDB::Instance()
{
	return reinterpret_cast<CDB*>(0x3046DA8);
}

void CDB::RequestLoadUserPoint(User *user, int type)
{
	reinterpret_cast<void(*)(CDB*, User*, int)>(0x5D140C)(this, user, type);
}

void CDB::SendSaveCharacterInfo(User *user, bool a, bool b)
{
	if (user && user->sd) {
		bool skip = false;
		{
			ScopedLock lock(user->ext.cs);
			if (!user->ext.famePointLoaded) {
				skip = true;
			}
		}
		if (skip) {
			CLog::Add(CLog::Red,
				L"User [%s]: can't save character: fame points not loaded yet (requesting load again)",
				user->sd->name);
			RequestLoadUserPoint(user, 5);
			return;
		}
	}
	reinterpret_cast<void(*)(CDB*, User*, bool, bool)>(0x5BAA64)(this, user, a, b);
}

void CDB::RequestLoadNavitAdvent(int dbId)
{
	if (Config::Instance()->custom->disableNavitSystem) return;
	reinterpret_cast<void(*)(CDB*, int)>(0x47EFC4)(this, dbId);
}

void CDB::RequestSaveNavitAdvent(User *user)
{
	if (Config::Instance()->custom->disableNavitSystem) return;
	reinterpret_cast<void(*)(CDB*, User*)>(0x47F0CC)(this, user);
}

bool CDB::ReplyExPacket(class CDBSocket *socket, const unsigned char *packet)
{
	GUARDED;

	UINT16 opcode = 0;
	const unsigned char *packetData = Disassemble(packet, "h", &opcode);
	bool(*handler)(class CDBSocket*, const unsigned char*);
	if (opcode > 0xAA) {
		handler = reinterpret_cast<bool(**)(class CDBSocket*, const unsigned char*)>(0x3046EC8)[0];
	} else if (opcode == 0xAA) {
		handler = &ReplyExtPacket;
	} else {
		handler = reinterpret_cast<bool(**)(class CDBSocket*, const unsigned char*)>(0x3046ED0)[opcode];
	}
	return handler(socket, packetData);

}

bool CDB::ReplyExtPacket(class CDBSocket *socket, const unsigned char *packet)
{
	GUARDED;

	UINT16 opcode = 0;
	const unsigned char *packetData = Disassemble(packet, "h", &opcode);
	switch (opcode) {
	case CDBExtPacket::REPLY_TEST: return ReplyTest(socket, packetData);
	case CDBExtPacket::REPLY_GET_PREMIUM: return ReplyGetPremium(socket, packetData);
	case CDBExtPacket::REPLY_SET_PREMIUM: return ReplySetPremium(socket, packetData);
	default:
		CLog::Add(CLog::Red, L"Unknown ExtPacket opcode %04X", opcode);
		return false;
	}
}

void CDB::RequestTest(int i)
{
	GUARDED;
	WorldGuard guard;
	Send("chhd", 0xE4, 0x0102, CDBExtPacket::REQUEST_TEST, i);
}

bool CDB::ReplyTest(class CDBSocket *socket, const unsigned char *packet)
{
	GUARDED;

	int i = 0;
	Disassemble(packet, "d", &i);
	CLog::Add(CLog::Blue, L"TestPacket reply :) %d", i);
	return false;
}

void CDB::RequestGetPremium(int accountId)
{
	GUARDED;
	WorldGuard guard;
	Send("chhd", 0xE4, 0x0102, CDBExtPacket::REQUEST_GET_PREMIUM, accountId);
}

bool CDB::ReplyGetPremium(class CDBSocket *socket, const unsigned char *packet)
{
	GUARDED;

	int accountId = 0;
	UINT32 expireTime = 0;
	Disassemble(packet, "dd", &accountId, &expireTime);

	CSPointer<User> user(CObjectDB::GetObjectByAccountID<User>(accountId));
	if (user) user->SetPremiumExpireTime(expireTime);

	return false;
}

void CDB::RequestSetPremium(int accountId, UINT32 expireTime)
{
	GUARDED;
	WorldGuard guard;
	Send("chhdd", 0xE4, 0x0102, CDBExtPacket::REQUEST_SET_PREMIUM, accountId, expireTime);
}

bool CDB::ReplySetPremium(class CDBSocket *socket, const unsigned char *packet)
{
	GUARDED;

	int accountId = 0;
	UINT32 expireTime = 0;
	Disassemble(packet, "dd", &accountId, &expireTime);

	CSPointer<User> user(CObjectDB::GetObjectByAccountID<User>(accountId));
	if (user) user->SetPremiumExpireTime(expireTime);

	return false;
}


} // namespace l2server

