
#include <Server/CParty.h>
#include <Server/User.h>
#include <Common/CSharedCreatureData.h>

namespace l2server {

void CParty::SendRelationUpdates()
{
	xstd::vector<User*> members = GetAllMember();
	for (xstd::vector<User*>::iterator i = members.begin() ; i != members.end() ; ++i) {
		(*i)->SendRelationChanged(0);
	}
}

xstd::vector<User*> CParty::GetAllMember()
{
	xstd::vector<User*> result;
	reinterpret_cast<void(*)(CParty*, xstd::vector<User*>*)>(0x8028FC)(this, &result);
	return result;
}

xstd::vector<User*> CParty::GetAllMemberInRange(const FVector &location, const double distance)
{
	xstd::vector<User*> members(GetAllMember());
	xstd::vector<User*> ret;
	for (xstd::vector<User*>::const_iterator i(members.begin()) ; i != members.end() ; ++i) {
		if (!(*i)->sd->location.IsInRange(location, distance)) continue;
		ret.push_back(*i);
	}
	return ret;
}

CMultiPartyCommandChannel* CParty::GetMPCC()
{
	return reinterpret_cast<CMultiPartyCommandChannel*(*)(CParty*)>(0x800DB8)(this);
}

User* CParty::GetMaster()
{
	return reinterpret_cast<User*(*)(CParty*)>(0x800970)(this);
}

void CParty::Dismiss(bool b)
{
	reinterpret_cast<void(*)(CParty*, bool)>(0x80888C)(this, b);
}

void CParty::Withdraw(User *user, bool b)
{
	reinterpret_cast<void(*)(CParty*, User*, bool)>(0x80933C)(this, user, b);
}

} // namespace l2server

