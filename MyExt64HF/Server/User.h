
#pragma once

#include <Server/CCreature.h>
#include <Server/Types.h>
#include <Common/CriticalSection.h>
#include <Common/Enum.h>
#include <Common/xstd.h>

namespace l2server {

class CMultiPartyCommandChannel;
class CParty;
class CUserSocket;
class CUserSocketReleaseVerifier;

class User : public CCreature {
public:
	class AcquireInfo {
	public:
		// ?
	};

	static const UINT64 vtable = 0xD99F18;

	class Ext {
	public:
		Ext();
		~Ext();

		CriticalSection cs;
		bool famePointLoaded;
		bool offlineTrade;
		CUserSocketReleaseVerifier *offlineTradeUserSocketReleaseVerifier;
		bool expOff;
		bool petExpOff;
		UINT32 premiumExpireTime;
	};

	/* 0x0B40 */ virtual void SetItemCrystalEquipPenalty_0x0B40(CrystalType type) {}
	/*        */ inline void SetItemCrystalEquipPenalty(CrystalType type) { SetItemCrystalEquipPenalty_0x0B40(type); }
	/* 0x0B48 */ virtual void SetItemCrystalOriginal_0x0B48(CrystalType type) {}
	/*        */ inline void SetItemCrystalOriginal(CrystalType type) { SetItemCrystalOriginal_0x0B48(type); }
	/* 0x0B50 */ virtual CrystalType GetItemCrystalOriginal_0x0B50() const { return CrystalTypeNone; }
	/*        */ inline CrystalType GetItemCrystalOriginal() const { return GetItemCrystalOriginal_0x0B50(); }
	/* 0x0B58 */ virtual int GetUserAccountId_0x0B58() const {}
	/*        */ inline int GetUserAccountId() const { return GetUserAccountId_0x0B58(); }
	/* 0x0B60 */ virtual int GetID_0x0B60() const {}
	/*        */ inline int GetID() const { return GetID_0x0B60(); }
	/* 0x0B68 */ virtual void SetCloseLater_0x0B68() {}
	/*        */ inline void SetCloseLater() { SetCloseLater_0x0B68(); }
	/* 0x0B70 */ virtual void OnTeleport_0x0B70() {}
	/*        */ inline void OnTeleport() { OnTeleport_0x0B70(); }
	/* 0x0B78 */ virtual void SendSkillList_0x0B78(CUserSocket*, bool) {}
	/*        */ inline void SendSkillList(CUserSocket *socket, bool b) { SendSkillList_0x0B78(socket, b); }
	/* 0x0B80 */ virtual bool GatherSkillAcquireList_0x0B80(xstd::map<CSkillKey, User::AcquireInfo>&, int&, EtcSkillAcquireType acquireType, bool) { return false; }
	/* 0x0B88 */ virtual void ValidateItemSkills_0x0B88() {}
	/* 0x0B90 */ virtual bool SummonCreature_0x0B90(const wchar_t*, int, const wchar_t*, INT64, int, int, CSkillInfo*) { return false;  }
	/* 0x0B98 */ virtual class CSummon* GetSummon_0x0B98() { return 0; }
	/*        */ inline class CSummon* GetSummon() { return GetSummon_0x0B98(); }
	/* 0x0BA0 */ virtual bool HandleItemManipulation_0x0BA0(int, class ItemManip[], class AtomicJob*, int, CPacketHelper<16384>*, bool) { return false; }
	/* 0x0BA8 */ virtual int GetExtraInventorySlots_0x0BA8() { return 0; }
	/*        */ inline int GetExtraInventorySlots() { return GetExtraInventorySlots_0x0BA8(); }
	/* 0x0BB0 */ virtual void vfn0x0BB0() {}
	/* 0x0BB8 */ virtual class CPledge* GetPledge_0x0BB8() { return 0; }
	/*        */ inline class CPledge* GetPledge() { return GetPledge_0x0BB8(); }
	/* 0x0BC0 */ virtual void SendUserInfo_0x0BC0(CUserSocket*) {}
	/*        */ inline void SendUserInfo(CUserSocket *socket = 0) { SendUserInfo_0x0BC0(socket); }
	/* 0x0BC8 */ virtual void SendCharInfo_0x0BC8(CUserSocket*, bool) {}
	/*        */ inline void SendCharInfo(CUserSocket *socket = 0, bool invisible = false) { SendCharInfo_0x0BC8(socket, invisible); }

	static void Init();

	User* Constructor(wchar_t *a, wchar_t *b, unsigned int c, unsigned int d, unsigned int e, int f, int g, int h, int i, int j, int k,
	                  int l, int m, double n, double o, unsigned int p, __int64 q, int r, int s, int t, unsigned int u, unsigned int v,
	                  int w, int x, int y, wchar_t *z, unsigned char *aa, unsigned int ab, unsigned int ac, unsigned int ad, int ae,
	                  int af, int ag, int ah, int ai, int aj, int ak, int al, int am, int an, bool ao, int ap, int aq, int ar, int as,
	                  int at, struct _SYSTEMTIME *au, int av, int aw, int ax, int ay, int az, int ba, int bb, int bc, int bd, int be,
	                  int bf, struct _SYSTEMTIME *bg);
	void* Destructor(bool freeMemory);

	// Subjob packet actions
	bool OnNpcCreateSubJobPacket(int npcId, int classId);
	bool OnNpcChangeSubJobPacket(int npcId, int classId);
	bool OnNpcRenewSubJobPacket(int npcId, int index, int classId);

	// Fix race condition problem with fame points
	void SetPoint(int type, int value);
	void AddPoint(int type, int value, bool b);
	void SetPointOnLoad(int type, int value);

	// Fixes class according to gender
	int FixClass(int classId);

	bool IsNowTrade();
	bool IsItemUsable();
	bool RideForEvent(const int classId, const int duration, const int unknown = 0);
	void TradeCancel();
	CMultiPartyCommandChannel* GetMPCC();
	void SendRelationChanged(CUserSocket *socket);
	CParty* GetParty();
	bool IsEnemyToWrapper(CCreature *creature);
	int GetRelationTo(User *user);
	bool IsInOlympiad();
	bool IsInBlockList(User &user);
	bool IsInBlockListOrOlympiadWithDisallowedTrade(User &user);
	void EnterWorld();
	void LeaveWorldEx(bool leaveForRaid);
	void SetNavitAdventPoint(int value, NavitPointChangeType type);
	void StartNavitAdventEffect(int i, bool b);
	void NavitPointInc();
	bool OpenOfflineTrade();
	bool CloseOfflineTrade();
	PrivateStoreType GetPrivateStoreType();
	void QuitPrivateStore(bool b);
	INT64 ExpIncEx(INT64 exp, bool b);
	void SetPremiumExpireTime(UINT32 expireTime);
	void SetPremiumLevel(int level);
	int GetPremiumLevel();
	void SetPremium(bool premium);
	void GivePremiumItem();
	void TimerExpiredEx(int id);
	void ResetNicknameAndColor();
	void OnDieDropItem(bool b, double d);
	static bool CheckCharacterName(const wchar_t *name, int country, bool allowLongerNames);
	void SetNicknameColor(int color);
	bool CheckPrivateStoreDistance();
	bool CanOpenPrivateStore(PrivateStoreType type);
	int SendPrivateStoreManageList(bool packageSale, class CUserSocket *socket);
	int SendPrivateStoreBuyManageList(class CUserSocket *socket);
	void SendRecipeStoreManageList();
	void QuitRecipeStore();
	bool RequestRecipeShopManageList(int recipeShop);
	void DeleteYongmaAbnormalStatus(bool b = true);
	void DeleteYongmaAbnormalStatusRidePet(bool b);
	void DeleteYongmaAbnormalStatusDismountFromPet(bool b);

	/* 0x1F28 */ unsigned char padding0x1F28[0x34F8 - 0x1F28];
	/* 0x34F8 */ int olympiadStatus;
	/* 0x34FC */ unsigned char padding0x34FC[0x39F0 - 0x34FC];
	/* 0x39F0 */ unsigned int nicknameColor;
	/* 0x39F4 */ unsigned char padding0x39FC[0x3F88 - 0x39F4];
	/* 0x3F88 */ Ext ext;
};

} // namespace l2server

