
#pragma once

#include <Server/MemoryObject.h>
#include <vector>

namespace l2server {

class CDB : public MemoryObject {
public:
	static void Init();

	static CDB* Instance();

	/* 0x0020 */ virtual void Send(const char *format, ...) { }

	void RequestLoadUserPoint(class User *user, int type);
	void SendSaveCharacterInfo(class User *user, bool a, bool b);
	void RequestLoadNavitAdvent(int dbId);
	void RequestSaveNavitAdvent(User *user);

	static bool ReplyExPacket(class CDBSocket *socket, const unsigned char *packet);
	static bool ReplyExtPacket(class CDBSocket *socket, const unsigned char *packet);

	void RequestTest(int i);
	static bool ReplyTest(class CDBSocket *socket, const unsigned char *packet);

	void RequestGetPremium(int accountId);
	static bool ReplyGetPremium(class CDBSocket *socket, const unsigned char *packet);

	void RequestSetPremium(int accountId, UINT32 expireTime);
	static bool ReplySetPremium(class CDBSocket *socket, const unsigned char *packet);
};

} // namespace l2server

