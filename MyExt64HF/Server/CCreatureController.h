
#pragma once

#include <Server/CIOObject.h>

namespace l2server {

class CNewAction;

class CCreatureController : public CIOObject {
public:
	static const UINT64 vtable = 0xBE0398;

	/* 0x0060 */ virtual CNewAction* GetCurrentAction() { return 0; }
	/* 0x0068 */ virtual void vfn0x0068() {}
	/* 0x0070 */ virtual void vfn0x0080() {}
	/* 0x0078 */ virtual void vfn0x0078() {}
	/* 0x0080 */ virtual void OnDamaged_0x0080() {}
	/* 0x0088 */ virtual void AddTimer2_0x0088() {}
};

} // namespace l2server

