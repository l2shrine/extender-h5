
#pragma once

#include <Server/CIOSocketEx.h>
#include <Windows.h>

namespace l2server {

class CSocket : public CIOSocketEx {
public:
	/* 0x00A8 */ virtual void SetAddress(struct in_addr) {}
	/* 0x00B0 */ virtual struct in_addr GetAddress() { return in_addr(); }
	/* 0x00B8 */ virtual void OnSending(unsigned char, unsigned short) {}

	/* 0x00B0 */ UINT64 socketHandleCopy;
	/* 0x00B8 */ in_addr clientIP;
	/* 0x00C0 */ unsigned char padding0x00C0[0x00D8 - 0x00C0];
	/* 0x00D8 */
};

} // namespace l2server

