
#pragma once

#include <Server/CStaticObject.h>

namespace l2server {

class CResidenceDecoVisible : public CStaticObject {
public:
	static const UINT64 vtable = 0xB29E48;
};

} // namespace l2server

