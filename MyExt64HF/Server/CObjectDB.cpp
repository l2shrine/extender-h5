
#include <Server/CObjectDB.h>
#include <Server/CItem.h>

namespace l2server {

CObjectDB* CObjectDB::Instance()
{
	return reinterpret_cast<CObjectDB*>(0x1258CD20);
}

int CObjectDB::GetClassIdFromName(const wchar_t *name) const
{
	return reinterpret_cast<int(*)(const CObjectDB*, const wchar_t*)>(0x7E1A48)(this, name);
}

CItem* CObjectDB::CreateItem(const int itemType) const
{
	return reinterpret_cast<CItem*(*)(const CObjectDB*, const int)>(0x7E2130)(this, itemType);
}

CObject* CObjectDB::GetObject(const int id)
{
	return reinterpret_cast<CObject*(*)(CObjectDB*, const int)>(0x41BE00)(reinterpret_cast<CObjectDB*>(0x1320F80), id);
}

CItem* CObjectDB::GetItem(const int id)
{
	CObject *object = GetObject(id);
	if (!object->IsItem()) return 0;
	return reinterpret_cast<CItem*>(object);
}

} // namespace l2server

