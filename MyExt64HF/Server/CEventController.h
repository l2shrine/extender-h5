
#pragma once

#include <Server/CStaticObject.h>

namespace l2server {

class CEventController : public CStaticObject {
public:
	static const UINT64 vtable = 0xC319C8;
};

} // namespace l2server

