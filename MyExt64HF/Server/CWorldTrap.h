
#pragma once

#include <Server/CTrap.h>

namespace l2server {

class CWorldTrap : public CTrap {
public:
	static const UINT64 vtable = 0xD7E2B8;
};

} // namespace l2server

