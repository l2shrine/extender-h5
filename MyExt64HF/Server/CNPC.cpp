
#include <Server/CNPC.h>
#include <Server/DropRate.h>
#include <Common/Enum.h>

namespace l2server {

void CNPC::Init()
{
	WriteMemoryQWORD(0xB5DDA0, FnPtr(&CNPC::Set_0x0B68_Wrapper));
	WriteMemoryQWORD(0xBBA750, FnPtr(&CNPC::Set_0x0B68_Wrapper));
	WriteMemoryQWORD(0xC242E0, FnPtr(&CNPC::Set_0x0B68_Wrapper));
	WriteMemoryQWORD(0xC98540, FnPtr(&CNPC::Set_0x0B68_Wrapper));
	WriteMemoryQWORD(0xCB1100, FnPtr(&CNPC::Set_0x0B68_Wrapper));
	WriteMemoryQWORD(0xCECB50, FnPtr(&CNPC::Set_0x0B68_Wrapper));
	WriteMemoryQWORD(0xCEE5E0, FnPtr(&CNPC::Set_0x0B68_Wrapper));
	WriteMemoryQWORD(0xD73510, FnPtr(&CNPC::Set_0x0B68_Wrapper));
	WriteMemoryQWORD(0xD7C2B0, FnPtr(&CNPC::Set_0x0B68_Wrapper));
	WriteMemoryQWORD(0xD7CED0, FnPtr(&CNPC::Set_0x0B68_Wrapper));
	WriteMemoryQWORD(0xD7EE20, FnPtr(&CNPC::Set_0x0B68_Wrapper));
	WriteMemoryQWORD(0xDDC7E0, FnPtr(&CNPC::Set_0x0B68_Wrapper));
	WriteMemoryQWORD(0xB5DDB0, FnPtr(&CNPC::Set_0x0B78_Wrapper));
	WriteMemoryQWORD(0xBBA760, FnPtr(&CNPC::Set_0x0B78_Wrapper));
	WriteMemoryQWORD(0xC242F0, FnPtr(&CNPC::Set_0x0B78_Wrapper));
	WriteMemoryQWORD(0xC98550, FnPtr(&CNPC::Set_0x0B78_Wrapper));
	WriteMemoryQWORD(0xCB1110, FnPtr(&CNPC::Set_0x0B78_Wrapper));
	WriteMemoryQWORD(0xCECB60, FnPtr(&CNPC::Set_0x0B78_Wrapper));
	WriteMemoryQWORD(0xCEE5F0, FnPtr(&CNPC::Set_0x0B78_Wrapper));
	WriteMemoryQWORD(0xD73520, FnPtr(&CNPC::Set_0x0B78_Wrapper));
	WriteMemoryQWORD(0xD7C2C0, FnPtr(&CNPC::Set_0x0B78_Wrapper));
	WriteMemoryQWORD(0xD7CEE0, FnPtr(&CNPC::Set_0x0B78_Wrapper));
	WriteMemoryQWORD(0xD7EE30, FnPtr(&CNPC::Set_0x0B78_Wrapper));
	WriteMemoryQWORD(0xDDC7F0, FnPtr(&CNPC::Set_0x0B78_Wrapper));

	DropRate::Init();
}

void CNPC::Set_0x0B68_Wrapper(ObjectFieldType objectFieldType, xstd::vector<ItemDropMultiStruct*> *itemDrop)
{
	DropRate::Apply(this, objectFieldType, itemDrop);
	reinterpret_cast<void(*)(CNPC*, ObjectFieldType, xstd::vector<ItemDropMultiStruct*>*)>(0x7A6B18)(this, objectFieldType, itemDrop);
}

void CNPC::Set_0x0B78_Wrapper(ObjectFieldType objectFieldType, xstd::vector<ItemDropStruct*> *itemDrop)
{
	DropRate::Apply(this, objectFieldType, itemDrop);
	reinterpret_cast<void(*)(CNPC*, ObjectFieldType, xstd::vector<ItemDropStruct*>*)>(0x7A6830)(this, objectFieldType, itemDrop);
}

} // namespace l2server
