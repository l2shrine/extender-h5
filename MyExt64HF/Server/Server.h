
#pragma once

#include <Common/CriticalSection.h>
#include <Common/Config.h>

namespace l2server {

class Server {
public:
	// Plugin interface
	class Plugin {
	public:
		// Socket initialization - called when user connects
		virtual void init(unsigned char *data, CriticalSection &cs) = 0;

		// Packet decryption - called when packet from user arrives
		virtual void decrypt(unsigned char *data, CriticalSection &cs, BYTE *packet, int packetLen, BYTE opcode) = 0;
	};

	// Static initialization
	static void Init();

	// Set debug on/off
	static void SetDebug(bool debug);

	// Check if debug is enabled
	static bool IsDebug();

	// Returns plugin
	static Plugin* GetPlugin();

	// Global TimerExpired
	static void TimerExpired(void *obj, int id);

protected:
	// Load additional stuff
	static void Load();

	// Set deadlock timeout in seconds
	static void DeadlockTimeout(UINT32 timeout);

	// Disable exit when authd can't be reached
	static void DisableNoAuthExit();

	// Disable sending mail to NCsoft
	static void DisableSendMail();

	// Hide numerous warnings
	static void HideWarnings();

	// Set shutdown duration in seconds
	static void SetShutdownSeconds(const int seconds);

	// Hook server start
	static void HookStart();

	// Actions to be executed on start
	static void StartHook(void *logger, int level, const wchar_t *fmt, const wchar_t *build);

	// Wrapper to create window
	static HWND CreateWindowEx(DWORD dwExStyle, LPCWSTR lpClassName, LPCWSTR lpWindowName, DWORD dwStyle, int X, int Y, int nWidth, int nHeight, HWND hWndParent, HMENU hMenu, HINSTANCE hInstance, LPVOID lpParam);

	// Hook load
	static void HookLoad();

	// Load hook, calls Load()
	static void LoadHook();

	// Disable setting thread core affinity (let operating system decide how to do it better, we're not in 2009)
	static void DontSetThreadCoreAffinity();

	// Initialize UTF-8 support
	static void InitUtf8Support();

	// Disable skill cooldown reset on skill acquire (antiexploit)
	static void DontResetCooldownOnSkillAcquire();

	// Skips check for is_magic == 3
	static void RelogKeepSongsDances();

	// Initialize clan restrictions
	static void InitClanRestrictions();

	// Is debug enabled?
	static bool debug;

	// Loaded plugin
	static Plugin *plugin;
};

} // namespace l2server

