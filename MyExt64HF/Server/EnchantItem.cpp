
#include <Server/EnchantItem.h>
#include <Server/CItem.h>
#include <Common/Parser.h>
#include <Common/Utils.h>
#include <Common/CLog.h>
#include <boost/lambda/lambda.hpp>
#include <boost/lambda/bind.hpp>
#include <map>

namespace l2server {

namespace {

std::map<int, double> chancesFighterWeapon;
std::map<int, double> chancesMageWeapon;
std::map<int, double> chancesNormalArmor;
std::map<int, double> chancesOnePieceArmor;
std::map<int, double> chancesEventWeapon;

class ParserImpl : public Parser {
public:
	ParserImpl() : lastMap(&chancesFighterWeapon), lastLevel(1)
	{
		weapon %= qi::lexeme[wide::string(L"weapon")];
		armor %= qi::lexeme[wide::string(L"armor")];
		fighter %= qi::lexeme[wide::string(L"fighter")][boost::lambda::var(lastMap) = &chancesFighterWeapon];
		mage %= qi::lexeme[wide::string(L"mage")][boost::lambda::var(lastMap) = &chancesMageWeapon];
		event %= qi::lexeme[wide::string(L"event")][boost::lambda::var(lastMap) = &chancesEventWeapon];
		normal %= qi::lexeme[wide::string(L"normal")][boost::lambda::var(lastMap) = &chancesNormalArmor];
		onepiece %= qi::lexeme[wide::string(L"onepiece")][boost::lambda::var(lastMap) = &chancesOnePieceArmor];
		level %= qi::lexeme[wide::string(L"level")];
		chance %= qi::lexeme[wide::string(L"chance")];
		weaponAssignment %= weapon > assign > (fighter | mage | event);
		armorAssignment %= armor > assign > (normal | onepiece);
		levelAssignment %= level > assign > integer[boost::lambda::var(lastLevel) = boost::lambda::_1 - 1];
		chanceAssignment %= chance > assign > decimal[boost::lambda::bind(&std::map<int, double>::operator[], boost::lambda::var(lastMap), boost::lambda::var(lastLevel)) = boost::lambda::_1];
		assignment %= weaponAssignment | armorAssignment | levelAssignment | chanceAssignment;
		start %= -bom > * assignment > qi::eoi;
	}

	qi::rule<Iterator> weapon, armor, fighter, mage, event, normal, onepiece, level, chance;
	qi::rule<Iterator> assignment, weaponAssignment, armorAssignment, levelAssignment, chanceAssignment;
	std::map<int, double> *lastMap;
	int lastLevel;
};

} // namespace

void EnchantItem::Init()
{
	WriteMemoryBYTES(0x70E67F + 0x0, "\x48\x89\xd9", 3); // mov rcx, rbx
	WriteMemoryBYTES(0x70E67F + 0x3, "\x48\x89\xea", 3); // mov rdx, rbp
	WriteInstructionCall(0x70E67F + 0x6, FnPtr(GetFighterWeaponChance)); // get chance
	WriteMemoryBYTES(0x70E67F + 0xB, "\xf2\x0f\x10\xf0", 4); // movsd xmm6, xmm0
	WriteInstructionJmp(0x70E67F + 0xF, 0x70E69D, 0); // jump to 70E69D

	WriteMemoryBYTES(0x70E5F6 + 0x0, "\x48\x89\xd9", 3); // mov rcx, rbx
	WriteMemoryBYTES(0x70E5F6 + 0x3, "\x48\x89\xea", 3); // mov rdx, rbp
	WriteInstructionCall(0x70E5F6 + 0x6, FnPtr(GetMageWeaponChance)); // get chance
	WriteMemoryBYTES(0x70E5F6 + 0xB, "\xf2\x0f\x10\xf0", 4); // movsd xmm6, xmm0
	WriteInstructionJmp(0x70E5F6 + 0xF, 0x70E614, 0); // jump to 70E614

	WriteMemoryBYTES(0x70E708 + 0x0, "\x48\x89\xd9", 3); // mov rcx, rbx
	WriteMemoryBYTES(0x70E708 + 0x3, "\x44\x88\xe2", 3); // mov dl, r12b
	WriteInstructionCall(0x70E708 + 0x6, FnPtr(GetArmorChance));
	WriteMemoryBYTES(0x70E708 + 0xB, "\xf2\x0f\x10\xf0", 4); // movsd xmm6, xmm0
	WriteMemoryBYTES(0x70E708 + 0xF, "\xf2\x0f\x10\x3d\x69\xa9\x41\x00", 8); // movsd xmm7, cs:0xB29088
	WriteMemoryBYTES(0x70E708 + 0x17, "\x4c\x8d\x25\xda\x18\xcf\xff", 7); // lea r12, cs:0x400000
	NOPMemory(0x70E708 + 0x1e, 84);
	NOPMemory(0x70E7C7, 9);
}

void EnchantItem::Load()
{
	CLog::Add(CLog::Blue, L"Reading ..\\Script\\itemenchant.txt");
	if (!ParserImpl().Parse(L"..\\Script\\itemenchant.txt", true)) {
		CLog::Add(CLog::Red, L"Failed to load itemenchant.txt");
	} else {
		CLog::Add(CLog::Blue, L"Loaded ..\\Script\\itemenchant.txt");
	}
}

double __cdecl EnchantItem::GetFighterWeaponChance(const int level, CItem *item)
{
	GUARDED;

	if (item->itemInfo->crystalType == 8) return GetEventWeaponChance(level);
	std::map<int, double>::const_iterator i(chancesFighterWeapon.find(level));
	if (i == chancesFighterWeapon.end()) {
		CLog::Add(CLog::Red, L"Chance for enchant fighter weapon +%d -> +%d not found, assuming zero chance", level, level + 1);
		return 0.0;
	} else {
		return i->second;
	}
}

double __cdecl EnchantItem::GetMageWeaponChance(const int level, CItem * item)
{
	GUARDED;

	if (item->itemInfo->crystalType == 8) return GetEventWeaponChance(level);
	std::map<int, double>::const_iterator i(chancesMageWeapon.find(level));
	if (i == chancesMageWeapon.end()) {
		CLog::Add(CLog::Red, L"Chance for enchant mage weapon +%d -> +%d not found, assuming zero chance", level, level + 1);
		return 0.0;
	} else {
		return i->second;
	}
}

double __cdecl EnchantItem::GetArmorChance(const int level, const bool onePiece)
{
	GUARDED;

	const std::map<int, double> &data(onePiece ? chancesOnePieceArmor : chancesNormalArmor);
	std::map<int, double>::const_iterator i(data.find(level));
	if (i == data.end()) {
		CLog::Add(CLog::Red, L"Chance for enchant %s armor +%d -> +%d not found, assuming zero chance",
			onePiece ? "one piece" : "normal", level, level + 1);
		return 0.0;
	} else {
		return i->second;
	}
}

double __cdecl EnchantItem::GetEventWeaponChance(const int level)
{
	GUARDED;

	std::map<int, double>::const_iterator i(chancesEventWeapon.find(level));
	if (i == chancesEventWeapon.end()) {
		CLog::Add(CLog::Red, L"Chance for enchant event weapon +%d -> +%d not found, assuming zero chance", level, level + 1);
		return 0.0;
	} else {
		return i->second;
	}
}

} // namespace l2server
