
#pragma once

#include <Common/xstd.h>

namespace l2server {

class CNPC;
enum ObjectFieldType;
class ItemDropMultiStruct;
class ItemDropStruct;

class DropRate {
public:
	static void Init();
	static void Apply(const CNPC *npc, const enum ObjectFieldType objectFieldType, xstd::vector<ItemDropMultiStruct*> *itemDrop);
	static void Apply(const CNPC *npc, const enum ObjectFieldType objectFieldType, xstd::vector<ItemDropStruct*> *itemDrop);
};

} // namespace l2server
