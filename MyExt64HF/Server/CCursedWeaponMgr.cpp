
#include <Server/CCursedWeaponMgr.h>

namespace l2server {

CCursedWeaponMgr* CCursedWeaponMgr::Instance()
{
	return reinterpret_cast<CCursedWeaponMgr*(*)()>(0x5B86C8)();
}

} // namespace l2server

