
#pragma once

#include <Server/User.h>
#include <Common/CSPointer.h>

namespace l2server {

class CParty;

class CMultiPartyCommandChannelManager {
public:
	static void Init();

	void Join(CSPointer<User> &owner, CSPointer<User> &target);
	void Oust(CSPointer<User> &owner, CSPointer<User> &target);
	void WithdrawInternal(CParty *party);

};

} // namespace l2server

