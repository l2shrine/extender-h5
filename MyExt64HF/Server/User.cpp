
#include <Server/User.h>
#include <Server/CDB.h>
#include <Server/CUserSocketReleaseVerifier.h>
#include <Server/CUserSocket.h>
#include <Server/CParty.h>
#include <Server/NFontCheck.h>
#include <Server/global.h>
#include <time.h>
#include <Common/Utils.h>
#include <Common/CSharedCreatureData.h>
#include <Common/CLog.h>
#include <Common/Enum.h>
#include <Common/Config.h>

namespace l2server {

void User::Init()
{
	// Fix User size
	WriteMemoryDWORD(0x63279C + 1, sizeof(User));
	WriteMemoryDWORD(0x600B7E + 8, sizeof(User));

	// Init and destroy ext
	WriteInstructionCall(0x632ACE, FnPtr(&User::Constructor));
	WriteInstructionCall(0x994D2C, FnPtr(&User::Destructor));

	// Subjob packet actions
	WriteMemoryQWORD(0xD9AC58, FnPtr(&User::OnNpcCreateSubJobPacket));
	WriteMemoryQWORD(0xD9AC60, FnPtr(&User::OnNpcChangeSubJobPacket));
	WriteMemoryQWORD(0xD9AC68, FnPtr(&User::OnNpcRenewSubJobPacket));

	// Fix race condition problem with fame points
	WriteInstructionCall(0x5EF874 + 0x21D, FnPtr(&User::AddPoint));
	WriteInstructionCall(0x5EF874 + 0x2F6, FnPtr(&User::AddPoint));
	WriteInstructionCall(0x9902D4 + 0x1E5, FnPtr(&User::AddPoint));
	WriteInstructionCall(0x5EF68C + 0x15E, FnPtr(&User::SetPointOnLoad));
	WriteInstructionCall(0x5EF874 + 0x34B, FnPtr(&User::SetPoint));
	WriteInstructionCall(0x9567FC + 0xCC, FnPtr(&User::SetPoint));
	WriteInstructionCall(0x95BB84 + 0xAC, FnPtr(&User::SetPoint));
	WriteInstructionCall(0x95C14C + 0x8F, FnPtr(&User::SetPoint));
	WriteInstructionCall(0x98FF9C + 0x112, FnPtr(&User::SetPoint));

	// Friendly command channel
	WriteMemoryQWORD(User::vtable + 0x410, FnPtr(&User::IsEnemyToWrapper));
	WriteInstructionCall(0x568720 + 0x141, FnPtr(&User::GetRelationTo));
	WriteInstructionCall(0x5A3BAC + 0x497, FnPtr(&User::GetRelationTo));
	WriteInstructionCall(0x96654C + 0x17A, FnPtr(&User::GetRelationTo));
	WriteInstructionCall(0x966714 + 0x1D1, FnPtr(&User::GetRelationTo));
	WriteInstructionCall(0x966C24 + 0x9, FnPtr(&User::GetRelationTo));
	WriteInstructionCall(0x9680F8 + 0x3DF, FnPtr(&User::GetRelationTo));
	WriteInstructionCall(0x968614 + 0x20B, FnPtr(&User::GetRelationTo));
	WriteInstructionCall(0x9CF8C8 + 0x2A, FnPtr(&User::GetRelationTo));
	WriteInstructionCall(0x9D0DB4 + 0x30, FnPtr(&User::GetRelationTo));
	WriteInstructionCall(0x9D1368 + 0xED, FnPtr(&User::GetRelationTo));

	// Disallow trade on olympiad
	WriteInstructionCall(0x97E0CB, FnPtr(&User::IsInBlockListOrOlympiadWithDisallowedTrade));

	// Disable navit system
	WriteInstructionCall(0x488BA4, FnPtr(&User::SetNavitAdventPoint));
	WriteInstructionCall(0x4C800B, FnPtr(&User::SetNavitAdventPoint));
	WriteInstructionCall(0x5905AD, FnPtr(&User::SetNavitAdventPoint));
	WriteInstructionCall(0x5FA9BA, FnPtr(&User::SetNavitAdventPoint));
	WriteInstructionCall(0x95BC97, FnPtr(&User::SetNavitAdventPoint));
	WriteInstructionCall(0x99796C, FnPtr(&User::SetNavitAdventPoint));
	WriteInstructionCall(0x5FAA8A, FnPtr(&User::StartNavitAdventEffect));
	WriteInstructionCall(0x997986, FnPtr(&User::StartNavitAdventEffect));
	WriteInstructionCall(0x7DC0F3, FnPtr(&User::NavitPointInc));
	WriteInstructionCall(0x7DE652, FnPtr(&User::NavitPointInc));
	WriteInstructionCall(0x7DEB63, FnPtr(&User::NavitPointInc));
	WriteInstructionCall(0x7E09FB, FnPtr(&User::NavitPointInc));

	// Offline trade
	WriteInstructionCall(0x987AEF, FnPtr(&User::QuitPrivateStore));
	WriteInstructionCall(0x985C4A, FnPtr(&User::QuitPrivateStore));

	// Expoff
	WriteMemoryQWORD(0xD9A4D8, FnPtr(&User::ExpIncEx));

	// Enter and leave world
	WriteInstructionCall(0x62A18B, FnPtr(&User::EnterWorld));
	WriteMemoryQWORD(0xD9A7C0, FnPtr(&User::LeaveWorldEx));	

	// Timer
	WriteMemoryQWORD(0xD99F38, FnPtr(&User::TimerExpiredEx));

	// Don't drop items on airship
	WriteInstructionCall(0x96BF22, FnPtr(&User::OnDieDropItem));

	// Custom character name checks
	WriteInstructionJmp(0x94FC0C, FnPtr(CheckCharacterName));

	// Private store checks
	WriteInstructionCall(0x99BA99, FnPtr(&User::CanOpenPrivateStore));
	WriteInstructionCall(0x99BB61, FnPtr(&User::CanOpenPrivateStore));
	WriteInstructionCall(0x99BC29, FnPtr(&User::CanOpenPrivateStore));
	WriteInstructionCall(0x9CA26D, FnPtr(&User::RequestRecipeShopManageList));
	WriteInstructionCall(0x9CA410, FnPtr(&User::RequestRecipeShopManageList));

	// Mount/dismount keep songs/dances
	WriteInstructionCall(0x9C6032, FnPtr(&User::DeleteYongmaAbnormalStatusRidePet));
	WriteInstructionCall(0x9C686A, FnPtr(&User::DeleteYongmaAbnormalStatusDismountFromPet));
}

User* User::Constructor(wchar_t *a, wchar_t *b, unsigned int c, unsigned int d, unsigned int e, int f, int g, int h, int i, int j, int k,
                        int l, int m, double n, double o, unsigned int p, __int64 q, int r, int s, int t, unsigned int u, unsigned int v,
                        int w, int x, int y, wchar_t *z, unsigned char *aa, unsigned int ab, unsigned int ac, unsigned int ad, int ae,
                        int af, int ag, int ah, int ai, int aj, int ak, int al, int am, int an, bool ao, int ap, int aq, int ar, int as,
                        int at, struct _SYSTEMTIME *au, int av, int aw, int ax, int ay, int az, int ba, int bb, int bc, int bd, int be,
                        int bf, struct _SYSTEMTIME *bg)
{
	User *result = reinterpret_cast<User*(*)(User*, wchar_t *, wchar_t *, unsigned int, unsigned int, unsigned int, int, int, int, int,
		int, int, int, int, double, double, unsigned int, __int64, int, int, int, unsigned int, unsigned int, int, int, int, wchar_t *,
		unsigned char *, unsigned int, unsigned int, unsigned int, int, int, int, int, int, int, int, int, int, int, bool, int, int, int,
		int, int, struct _SYSTEMTIME *, int, int, int, int, int, int, int, int, int, int, int, struct _SYSTEMTIME*)>(0x990C1C)(
		this, a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v, w, x, y, z, aa, ab, ac, ad, ae, af, ag, ah, ai, aj,
		ak, al, am, an, ao, ap, aq, ar, as, at, au, av, aw, ax, ay, az, ba, bb, bc, bd, be, bf, bg);
	new (&result->ext) Ext();
	return result;
}

void* User::Destructor(bool freeMemory)
{
	ext.~Ext();
	return reinterpret_cast<void*(*)(User*, bool)>(0x992058)(this, freeMemory);
}

User::Ext::Ext() : famePointLoaded(false), offlineTrade(false), offlineTradeUserSocketReleaseVerifier(0), expOff(false), petExpOff(false), premiumExpireTime(0)
{
}

User::Ext::~Ext()
{
}

int User::FixClass(int classId)
{
	// Fix soul breaker and soulhound class ID according to gender
	if (sd->gender == GenderFemale) {
		if (classId == ClassMSoulBreaker) classId = ClassFSoulBreaker;
		else if (classId == ClassMSoulHound) classId = ClassFSoulHound;
	} else {
		if (classId == ClassFSoulBreaker) classId = ClassMSoulBreaker;
		else if (classId == ClassFSoulHound) classId = ClassMSoulHound;
	}
	return classId;
}

bool User::OnNpcCreateSubJobPacket(int npcId, int classId)
{
	// Fix class by gender
	return reinterpret_cast<bool(*)(User*, int, int)>(0x98A1CC)(this, npcId, FixClass(classId));
}

bool User::OnNpcChangeSubJobPacket(int npcId, int classId)
{
	// Fix class by gender
	return reinterpret_cast<bool(*)(User*, int, int)>(0x98A454)(this, npcId, FixClass(classId));
}

bool User::OnNpcRenewSubJobPacket(int npcId, int index, int classId)
{
	// Fix class by gender
	return reinterpret_cast<bool(*)(User*, int, int, int)>(0x98A5DC)(this, npcId, index, FixClass(classId));
}

void User::SetPoint(int type, int value)
{
	if (type == 5) {
		ScopedLock lock(ext.cs);
		if (!ext.famePointLoaded) {
			CLog::Add(CLog::Red, L"User [%s]: can't set fame points: not loaded yet (requesting load again)", sd->name);
			CDB::Instance()->RequestLoadUserPoint(this, 5);
			return;
		}
	}
	reinterpret_cast<void(*)(User*, int, int)>(0x956560)(this, type, value);
}

void User::AddPoint(int type, int value, bool b)
{
	if (type == 5) {
		ScopedLock lock(ext.cs);
		if (!ext.famePointLoaded) {
			CLog::Add(CLog::Red, L"User [%s]: can't add fame points: not loaded yet (requesting load again)", sd->name);
			CDB::Instance()->RequestLoadUserPoint(this, 5);
			return;
		}
	}
	reinterpret_cast<void(*)(User*, int, int, bool)>(0x95C14C)(this, type, value, b);
}

void User::SetPointOnLoad(int type, int value)
{
	reinterpret_cast<void(*)(User*, int, int)>(0x956560)(this, type, value);
	if (this && type == 5) {
		ScopedLock lock(ext.cs);
		ext.famePointLoaded = true;
	}
}

bool User::IsNowTrade()
{
	return reinterpret_cast<bool(*)(User*)>(0x95F480)(this);
}

bool User::IsItemUsable()
{
	return reinterpret_cast<bool(*)(User*)>(0x997FC8)(this);
}

bool User::RideForEvent(const int classId, const int duration, const int unknown)
{
	return reinterpret_cast<bool(*)(User*, const int, const int, const int)>(0x9C6640)(this, classId, duration, unknown);
}

void User::TradeCancel()
{
	reinterpret_cast<void(*)(User*)>(0x95F228)(this);
}

CMultiPartyCommandChannel* User::GetMPCC()
{
	return reinterpret_cast<CMultiPartyCommandChannel*(*)(User*)>(0x95C214)(this);
}

void User::SendRelationChanged(CUserSocket *socket)
{
	reinterpret_cast<void(*)(User*, CUserSocket*)>(0x9D1368)(this, socket);
}

CParty* User::GetParty()
{
	return reinterpret_cast<CParty*(*)(User*)>(0x95C2FC)(this);
}

bool User::IsEnemyToWrapper(CCreature *creature)
{
	bool result = reinterpret_cast<bool(*)(User*, CCreature*)>(0x966714)(this, creature);
	if (!Config::Instance()->fixes->commandChannelFriendly) return result;
	CParty *party = GetParty();
	CMultiPartyCommandChannel *mpcc = GetMPCC();
	User *user = creature->GetUserOrMaster();
	if (user) {
		if (user == this) return false;
		if (party && user->GetParty() == party) return false;
		if (mpcc && user->GetMPCC() == mpcc) return false;
	}
	return result;
}

int User::GetRelationTo(User *user)
{
	int result = reinterpret_cast<int(*)(User*, User*)>(0x965FF4)(this, user);
	if (!Config::Instance()->fixes->commandChannelFriendly) return result;
	CMultiPartyCommandChannel *mpcc = GetMPCC();
	if (mpcc && user->GetMPCC() == mpcc) result |= 0x20;
	return result;
}

bool User::IsInOlympiad()
{
	return olympiadStatus == 1 || olympiadStatus == 2;
}

bool User::IsInBlockList(User &user)
{
	return reinterpret_cast<bool(*)(User*, User&)>(0x970E04)(this, user);
}

bool User::IsInBlockListOrOlympiadWithDisallowedTrade(User &user)
{
	return (Config::Instance()->fixes->disallowTradeInOlympiad && IsInOlympiad()) || IsInBlockList(user);
}

void User::EnterWorld()
{
	if (sd->builder) {
		nicknameColor = 0xff00;
	}

	reinterpret_cast<void(*)(User*)>(0x995514)(this);

	CDB::Instance()->RequestGetPremium(GetUserAccountId());

	if (sd->builder) {
		reinterpret_cast<bool(*)(CUserSocket*, User*, wchar_t*)>(0x4A0554)(GetUserSocket(), this, L"//gmon");
	}
}

void User::LeaveWorldEx(bool leaveForRaid)
{
	reinterpret_cast<void(*)(User*, bool)>(0x99669C)(this, leaveForRaid);
}

void User::SetNavitAdventPoint(int value, NavitPointChangeType type)
{
	if (Config::Instance()->custom->disableNavitSystem) return;
	reinterpret_cast<void(*)(User*, int, NavitPointChangeType)>(0x488760)(this, value, type);
}

void User::StartNavitAdventEffect(int i, bool b)
{
	if (Config::Instance()->custom->disableNavitSystem) return;
	reinterpret_cast<void(*)(User*, int, bool)>(0x488AC0)(this, i, b);
}

void User::NavitPointInc()
{
	if (Config::Instance()->custom->disableNavitSystem) return;
	reinterpret_cast<void(*)(User*)>(0x488678)(this);
}

bool User::OpenOfflineTrade()
{
	switch (GetPrivateStoreType()) {
	case PrivateStoreTypeSell:
	case PrivateStoreTypeBuy:
	case PrivateStoreTypeManufacture:
	case PrivateStoreTypePackageSell:
		break;
	default:
		SendSystemMessage(Config::Instance()->server->name.c_str(), L"You can use offline store only when trading");
		return false;
	}

	{
		ScopedLock lock(ext.cs);
		if (ext.offlineTrade) return false;
		ext.offlineTrade = true;
		SetNicknameColor(0x9f9f9f);
	}

	if (CParty * party = GetParty()) {
		if (party->GetMaster() == this) {
			party->Dismiss(true);
		} else {
			party->Withdraw(this, true);
		}
	}

	if (IsNowTrade()) TradeCancel();

	if (CUserSocket * socket = GetUserSocket()) {
		socket->GracefulClose();
	}

	return true;
}

bool User::CloseOfflineTrade()
{
	ScopedLock lock(ext.cs);
	if (CUserSocket *socket = GetUserSocket()) {
		if (socket->ext.offlineTradeEnded) return true;
		if (ext.offlineTradeUserSocketReleaseVerifier) {
			ext.offlineTradeUserSocketReleaseVerifier->AddTimer(100, 3);
			ext.offlineTradeUserSocketReleaseVerifier = 0;
			socket->ext.offlineTradeEnded = true;
			return true;
		}
	}
	return false;
}

PrivateStoreType User::GetPrivateStoreType()
{
	return sd->privateStoreType;
}

void User::QuitPrivateStore(bool b)
{
	reinterpret_cast<void(*)(User*, int)>(0x954A14)(this, b);
	if (ext.offlineTrade) CloseOfflineTrade();
}

INT64 User::ExpIncEx(INT64 exp, bool b)
{
	if (ext.expOff && exp > 0) exp = 0;
	return reinterpret_cast<INT64(*)(User*, INT64, bool)>(0x9450FC)(this, exp, b);
}

void User::SetPremiumExpireTime(UINT32 expireTime)
{
	ScopedLock lock(ext.cs);
	ext.premiumExpireTime = expireTime;
	SetPremium(expireTime > time(0));
}

void User::SetPremiumLevel(int level)
{
	reinterpret_cast<void(*)(User*, int)>(0x4874EC)(this, level);
}

int User::GetPremiumLevel()
{
	return reinterpret_cast<int(*)(User*)>(0x487424)(this);
}

void User::SetPremium(bool premium)
{
	SetPremiumLevel(premium ? 1 : 0);
	sd->isPremiumUser = premium;
	if (CUserSocket *socket = GetUserSocket()) socket->Send("chdc", 0xFE, 0xD9, objectId, premium ? 1 : 0);
	GivePremiumItem();
}

void User::GivePremiumItem()
{
	reinterpret_cast<void(*)(User*)>(0x4878EC)(this);
}

void User::TimerExpiredEx(int id)
{
	reinterpret_cast<void(*)(User*, int)>(0x996CBC)(this, id);

	int proposedPremiumLevel(ext.premiumExpireTime > time(0) ? 1 : 0);
	if (GetPremiumLevel() != proposedPremiumLevel) SetPremium(proposedPremiumLevel);
}

void User::ResetNicknameAndColor()
{
	reinterpret_cast<void(*)(User*)>(0x958B38)(this);
}

void User::OnDieDropItem(bool b, double d)
{
	if (IsOnAirship()) return;
	reinterpret_cast<void(*)(User*, bool, double)>(0x99F4F4)(this, b, d);
}

bool User::CheckCharacterName(const wchar_t *name, int country, bool allowLongerNames)
{
	GUARDED;

	int len = wcslen(name);
	int maxLen = (allowLongerNames || country == COUNTRY_THAILAND) ? 24 : 16;
	if (len < 1 || len > maxLen) {
		CLog::Add(CLog::Red, L"[CheckCharacterName] Invalid Length : length=%d", len);
		return false;
	}
	for (size_t i = 0 ; i < len ; ++i) {
		if ((name[i] >= 'a' && name[i] <= 'z')
			|| (name[i] >= 'A' && name[i] <= 'Z')
			|| (name[i] >= '0' && name[i] <= '9')) {

			maxLen -= 1;
		} else {
			if (global::fontcheck && !NFontCheck::Instance()->CheckCode(name[i])) {
				CLog::Add(CLog::Red, L"[CheckCharacterName] Gly FontCheck Failed : char=%c(%d), name=[%s]", name[i], int(name[i]), name);
				return false;
			}

			if (Config::Instance()->charsets->allowedCharacterNameLetters.count(name[i])) {
				if (name[i] >= 0x0530) {
					maxLen -= 2;
				} else {
					maxLen -= 1;
				}
			} else if (country == COUNTRY_KOREA) {
				if (name[i] >= 0xAC00 && name[i] <= 0xD7AF) { // korean letters
					maxLen -= 2;
				} else {
					CLog::Add(CLog::Red, L"[CheckCharacterName] Invalid CodeSet : char=%c(%d), name=[%s]", name[i], int(name[i]), name);
					return false;
				}
			} else if (country == COUNTRY_JAPAN || country == COUNTRY_TAIWAN || country == COUNTRY_CHINA) {
				if ((name[i] >= 0x3041 && name[i] <= 0x3096) // katakana
					|| (name[i] >= 0x30A1 && name[i] <= 0x30FC) // hiragana
					|| (name[i] >= 0x4E00 && name[i] <= 0x9FA5)) { // cjk unified ideographs

					maxLen -= 2;
				} else {
					CLog::Add(CLog::Red, L"[CheckCharacterName] Invalid CodeSet : char=%c(%d), name=[%s]", name[i], int(name[i]), name);
					return false;
				}
			} else if (country == COUNTRY_THAILAND) {
				if ((name[i] >= 0x0E01 && name[i] <= 0x0E3A) // thai unicode letters
					|| (name[i] >= 0xE3F && name[i] <= 0xE5B)) { // thai unicode letters
					maxLen -= 2;
				} else {
					CLog::Add(CLog::Red, L"[CheckCharacterName] Invalid CodeSet : char=%c(%d), name=[%s]", name[i], int(name[i]), name);
					return false;
				}
			} else if (country == COUNTRY_RUSSIA) {
				if (name[i] >= 0x0410 && name[i] <= 0x044F) { // cyrillic letters
					maxLen -= 1;
				} else {
					CLog::Add(CLog::Red, L"[CheckCharacterName] Invalid CodeSet : char=%c(%d), name=[%s]", name[i], int(name[i]), name);
					return false;
				}
			} else {
				CLog::Add(CLog::Red, L"[CheckCharacterName] Invalid CodeSet : char=%c(%d), name=[%s]", name[i], int(name[i]), name);
				return false;
			}
		}
		if (maxLen < 0) {
			CLog::Add(CLog::Red, L"[CheckCharacterName] Invalid Length : length=%d, count=%d", len, i);
			return false;
		}
	}
	return true;
}

void User::SetNicknameColor(int color)
{
	nicknameColor = color;
	SendCharInfo();
}

bool User::CheckPrivateStoreDistance()
{
	GUARDED;

	double distance = Config::Instance()->custom->minShopDistance;
	if (distance < 0) return true;

	xstd::vector<CSafePointer2<CCreature> > creatures;
	xstd::vector<double> distances;
	GatherNeighborCreature(GetPosition(), -1, -1, false, creatures, distances, int(distance), -128, 128, SkillAffectObjectTypeNone, this, this);
	distance *= distance;
	for (size_t i = 0 ; i < creatures.size() ; ++i) {
		if (distances[i] >= distance) continue;
		CSPointer<CCreature> creatureSP = creatures[i].FindObjectSP();
		if (!creatureSP || !creatureSP->IsUser()) continue;
		User * user = creatureSP->CastUser();
		switch (user->sd->privateStoreType) {
		case PrivateStoreTypeSell:
		case PrivateStoreTypeBuy:
		case PrivateStoreTypeManufacture:
		case PrivateStoreTypePackageSell:
			SendSystemMessage(Config::Instance()->server->name.c_str(), L"You can't open private store here, you must find some empty spot");
			return false;
		}
	}

	return true;
}

bool User::CanOpenPrivateStore(PrivateStoreType type)
{
	if (!CheckPrivateStoreDistance()) return false;
	return reinterpret_cast<bool(*)(User*, PrivateStoreType)>(0x99B6A8)(this, type);
}

int User::SendPrivateStoreManageList(bool packageSale, CUserSocket *socket)
{
	return reinterpret_cast<int(*)(User*, bool, CUserSocket*)>(0x980294)(this, packageSale, socket);
}

int User::SendPrivateStoreBuyManageList(CUserSocket *socket)
{
	return reinterpret_cast<int(*)(User*, CUserSocket*)>(0x96DDA4)(this, socket);
}

void User::SendRecipeStoreManageList()
{
	reinterpret_cast<void(*)(User*)>(0x9A3AB8)(this);
}

void User::QuitRecipeStore()
{
	reinterpret_cast<void(*)(User*)>(0x99822C)(this);
}

bool User::RequestRecipeShopManageList(int recipeShop)
{
	if (!CheckPrivateStoreDistance()) return false;
	return reinterpret_cast<bool(*)(User*, int)>(0x9A6F5C)(this, recipeShop);
}

void User::DeleteYongmaAbnormalStatus(bool b)
{
	reinterpret_cast<void(*)(User*, bool)>(0x9C5C8C)(this, b);
}

void User::DeleteYongmaAbnormalStatusRidePet(bool b)
{
	if (Config::Instance()->server->mountKeepSongsDances) return;
	DeleteYongmaAbnormalStatus();
}

void User::DeleteYongmaAbnormalStatusDismountFromPet(bool b)
{
	if (Config::Instance()->server->dismountKeepSongsDances) return;
	DeleteYongmaAbnormalStatus();
}

static void check()
{
	static_assert(offsetof(User, ext) == 0x3F88);
}

} // namespace l2server

