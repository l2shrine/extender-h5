
#include <Server/COlympiadDefaultSetting.h>
#include <Common/Utils.h>
#include <Common/Config.h>

namespace l2server {

const double COlympiadDefaultSetting::penaltyPointDivider = 3.0;

UINT32 &COlympiadDefaultSetting::olympiadMinimumCountTeam = *reinterpret_cast<UINT32 * const>(0x12591D70);
UINT32 &COlympiadDefaultSetting::olympiadMinimumCountNonclass = *reinterpret_cast<UINT32 * const>(0x12591D74);
UINT32 &COlympiadDefaultSetting::olympiadMinimumCountClass = *reinterpret_cast<UINT32 * const>(0x12591D78);

void COlympiadDefaultSetting::Init()
{
	// Set custom minimum entry counts
	WriteInstructionCall(0x7F79EB, FnPtr(&COlympiadDefaultSetting::Constructor));

	// Fix point penalty
	WriteAddress(0x7E9987 + 4, reinterpret_cast<UINT32>(&penaltyPointDivider));
}

COlympiadDefaultSetting* COlympiadDefaultSetting::Constructor()
{
	COlympiadDefaultSetting *result = reinterpret_cast<COlympiadDefaultSetting*(*)(COlympiadDefaultSetting*)>(0x7F6900)(this);
	olympiadMinimumCountTeam = Config::Instance()->olympiad->entryCountTeam;
	olympiadMinimumCountNonclass = Config::Instance()->olympiad->entryCountNonclass;
	olympiadMinimumCountClass = Config::Instance()->olympiad->entryCountClass;
	return result;
}

} // l2server

