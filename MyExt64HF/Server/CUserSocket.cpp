
#include <Server/CUserSocket.h>
#include <Server/RWLock.h>
#include <Server/global.h>
#include <Server/User.h>
#include <Common/CLog.h>
#include <Common/Utils.h>
#include <Common/Enum.h>
#include <Common/Config.h>
#include <sstream>
#include <map>

namespace l2server {

void CUserSocket::Init()
{
	// Extend size
	WriteMemoryDWORD(0xA0D0E8 + 1, sizeof(CUserSocket));
	WriteMemoryDWORD(0x9EF27A + 8, sizeof(CUserSocket));

	// Constructor and destructor
	WriteInstructionCall(0xA0D104, FnPtr(&CUserSocket::Constructor));
	WriteInstructionCall(0xA017AC, FnPtr(&CUserSocket::Destructor));

	// Authentication hack fix
	WriteInstructionCall(0xA10C4F, FnPtr(&CUserSocket::_LoginPacket));

	// To support voice commands
	WriteAddress(0xB14378 + 3, FnPtr(&CUserSocket::SayPacket2));

	// Offline trade
	WriteInstructionCall(0x46198A, FnPtr(&CUserSocket::SetCloseTimer)); // auth kick
	WriteInstructionCall(0x461B68, FnPtr(&CUserSocket::SetCloseTimer)); // auth kick
	WriteInstructionCall(0xA0D63A, FnPtr(&CUserSocket::SetCloseTimer)); // time expired
	WriteInstructionCall(0xA0D7FC, FnPtr(&CUserSocket::SetCloseTimer)); // week usage time expired
	WriteInstructionCall(0xA0D93B, FnPtr(&CUserSocket::SetCloseTimer)); // age restriction

	// Offline trade
	WriteInstructionCall(0xA0DB64, FnPtr(&CUserSocket::AddTimerOfflineTrade));

	// Private store distance
	WriteAddress(0xB14CB0 + 3, FnPtr(PrivateStoreListSet));
	WriteAddress(0xB15138 + 3, FnPtr(PrivateStoreBuyListSet));
	WriteAddress(0xB15660 + 3, FnPtr(RequestRecipeShopSetList));
}

CUserSocket* CUserSocket::Constructor(UINT64 handle)
{
	CUserSocket *result = reinterpret_cast<CUserSocket*(*)(CUserSocket*, UINT64)>(0xA0CCC4)(this, handle);
	new (&result->ext) Ext();
	return result;
}

void* CUserSocket::Destructor(bool freeMemory)
{
	ext.~Ext();
	return reinterpret_cast<void*(*)(CUserSocket*, bool)>(0xA0169C)(this, freeMemory);
}

CUserSocket::Ext::Ext() : offlineTradeEnded(false)
{
}

CUserSocket::Ext::~Ext()
{
}

void CUserSocket::ForceClose(int reason)
{
	reinterpret_cast<void(*)(CUserSocket*, int)>(0x9FCBF0)(this, reason);
}

std::wstring CUserSocket::GetIP() const
{
	std::wstringstream ws;
	ws << static_cast<int>(clientIP.S_un.S_un_b.s_b1)
		<< "." << static_cast<int>(clientIP.S_un.S_un_b.s_b2)
		<< "." << static_cast<int>(clientIP.S_un.S_un_b.s_b3)
		<< "." << static_cast<int>(clientIP.S_un.S_un_b.s_b4);
	return ws.str();
}

bool CUserSocket::_LoginPacket(const wchar_t *accountName, const int accountId, const int unknown1, const int unknown2, const int sessionId, const int unknown3, const int unknown4)
{
	// Check session ID - if zero, deny login
	if (!sessionId) {
		CLog::Add(CLog::Red,
			L"HACK? Malformed LoginPacket - sessionId = 0 (got accountName = %s, accountId = %d), "
			L"IP address = %s, check authd logs to see who it really was; disconnecting user",
			accountName, accountId, GetIP().c_str());
		return true;
	}

	// Try to find account ID in auth session map
	RWLockReadGuard lock(global::authSessionLock);
	xstd::map<int, int>::const_iterator i = global::authAccountSessionMap->find(accountId);
	if (i == global::authAccountSessionMap->end()) {
		// Not found, deny login
		CLog::Add(CLog::Red,
			L"HACK? Malformed LoginPacket - account not in map (got accountName = %s, accountId = %d, sessionId = %d), "
			L"IP address = %s, check authd logs to see who it really was; disconnecting user",
			accountName, accountId, sessionId, GetIP().c_str());
		return true;
	}
	if (i->second != sessionId) {
		// Found but with different sessionId, deny login
		CLog::Add(CLog::Red,
			L"HACK? Malformed LoginPacket - wrong sessionId = %d, should be %d (got accountName = %s, accountId = %d), "
			L"IP address = %s, check authd logs to see who it really was; disconnecting user",
			sessionId, i->second, accountName, accountId, sessionId, GetIP().c_str());
		return true;
	}
	lock.Unlock();

	// Call original _LoginPacket function
	return reinterpret_cast<bool(*)(CUserSocket*, const wchar_t*, const int, const int, const int, const int, const int)>(0xA0FE54)(
		this, accountName, accountId, unknown1, unknown2, sessionId, unknown3);
}

bool CUserSocket::VoiceCommand(const std::wstring &command)
{
	GUARDED;

	if (!Config::Instance()->voiceCommands->enabled) return false;

	// Check if command is legal voice command, if so, perform it's actions
	if (command == L"offline" && Config::Instance()->voiceCommands->offline) {
		user->OpenOfflineTrade();
		return true;
	} else if (command == L"expoff" && Config::Instance()->voiceCommands->expOnOff) {
		if (user->ext.expOff) {
			user->SendSystemMessage(Config::Instance()->server->name.c_str(), L"Experience gain already turned off");
		} else {
			user->ext.expOff = true;
			user->SendSystemMessage(Config::Instance()->server->name.c_str(), L"Experience gain turned off");
		}
		return true;
	} else if (command == L"expon" && Config::Instance()->voiceCommands->expOnOff) {
		if (user->ext.expOff) {
			user->ext.expOff = false;
			user->SendSystemMessage(Config::Instance()->server->name.c_str(), L"Experience gain turned on");
		} else {
			user->SendSystemMessage(Config::Instance()->server->name.c_str(), L"Experience gain already turned on");
		}
		return true;
	} else if (command == L"petexpoff" && Config::Instance()->voiceCommands->petExpOnOff) {
		if (user->ext.petExpOff) {
			user->SendSystemMessage(Config::Instance()->server->name.c_str(), L"Pet experience gain already turned off");
		} else {
			user->ext.petExpOff = true;
			user->SendSystemMessage(Config::Instance()->server->name.c_str(), L"Pet experience gain turned off");
		}
		return true;
	} else if (command == L"petexpon" && Config::Instance()->voiceCommands->petExpOnOff) {
		if (user->ext.petExpOff) {
			user->ext.petExpOff = false;
			user->SendSystemMessage(Config::Instance()->server->name.c_str(), L"Pet experience gain turned on");
		} else {
			user->SendSystemMessage(Config::Instance()->server->name.c_str(), L"Pet experience gain already turned on");
		}
		return true;
	}

	// Command isn't legal voice command, return false (say it in normal chat)
	return false;
}

bool CUserSocket::SayPacket2(const unsigned char *packet)
{
	GUARDED;

	int type = -1;
	wchar_t buffer[0x400];
	Disassemble(packet, "Sd", sizeof(buffer), buffer, &type);

	// Check if chat type is ALL and message starts with dot
	if (type == ChatTypeAll && buffer[0] == L'.') {
		// Extract first word (command)
		std::wstring command;
		for (wchar_t *i = buffer + 1; *i && *i != L' '; ++i) command.push_back(*i);
		if (!command.empty()) {
			// Call voice command, if it's voice command, return and don't say it
			if (VoiceCommand(command)) return false;
		}
	}

	// Say it
	return reinterpret_cast<bool(*)(CUserSocket*, const unsigned char*)>(0xA10DC4)(this, packet);
}

void CUserSocket::SetCloseTimer()
{
	if (user && user->CloseOfflineTrade()) return;
	reinterpret_cast<void(*)(CUserSocket*)>(0x9F82C8)(this);
}

void CUserSocket::AddTimerOfflineTrade(const int interval, const int id)
{
	if (ext.offlineTradeEnded) return;
	AddTimer(interval, id);
}

bool CUserSocket::PrivateStoreListSet(CUserSocket *socket, const unsigned char *data)
{
	GUARDED;

	int isPackageSale(0);
	Disassemble(data, "d", &isPackageSale);
	User *user(socket->GetUser());
	if (!user) return true;
	switch (user->GetPrivateStoreType()) {
	case PrivateStoreTypeSellSet:
		if (isPackageSale) return true;
		break;
	case PrivateStoreTypePackageSellSet:
		if (!isPackageSale) return true;
		break;
	default:
		return true;
	}
	if (reinterpret_cast<bool(*)(CUserSocket*, const unsigned char*)>(0x9F36DC)(socket, data)) return true;
	if (!user->CheckPrivateStoreDistance()) {
		user->QuitPrivateStore(false);
		user->SendPrivateStoreManageList(isPackageSale, socket);
	}
	return false;
}

bool CUserSocket::PrivateStoreBuyListSet(CUserSocket *socket, const unsigned char *data)
{
	GUARDED;

	User *user(socket->GetUser());
	if (!user) return true;
	if (user->GetPrivateStoreType() != PrivateStoreTypeBuySet) return true;
	if (reinterpret_cast<bool(*)(CUserSocket*, const unsigned char*)>(0x9F4530)(socket, data)) return true;
	if (!user->CheckPrivateStoreDistance()) {
		user->QuitPrivateStore(false);
		user->SendPrivateStoreBuyManageList(socket);
	}
	return false;
}

bool CUserSocket::RequestRecipeShopSetList(CUserSocket *socket, const unsigned char *data)
{
	GUARDED;

	User *user(socket->GetUser());
	if (!user) return true;
	if (user->GetPrivateStoreType() != PrivateStoreTypeManufactureSet) return true;
	if (reinterpret_cast<bool(*)(CUserSocket*, const unsigned char*)>(0xA0293C)(socket, data)) return true;
	if (!user->CheckPrivateStoreDistance()) {
		user->QuitRecipeStore();
		user->SendRecipeStoreManageList();
	}
	return false;
}

static void check()
{
	static_assert(offsetof(CUserSocket, ext) == 0x0CE0);
}

} // namespace l2server

