
#pragma once

#include <Server/CSummon.h>

namespace l2server {

class CPet : public CSummon {
public:
	static const UINT64 vtable = 0xCEDA78;

	static void Init();

	INT64 ExpIncEx(INT64 exp, bool b);
};

} // namespace l2server

