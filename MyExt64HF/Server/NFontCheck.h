
#pragma once

namespace l2server {

class NFontCheck {
public:
	static NFontCheck* Instance();
	bool CheckCode(int code);
};

} // namespace l2server
