
#pragma once

#include <Common/xstd.h>

namespace l2server {

class CHTMLCacheManager {
public:
	static void Init();

	bool GetHTMLFile(const wchar_t *filename, xstd::wstring &output, int language);
};

} // namespace l2server

