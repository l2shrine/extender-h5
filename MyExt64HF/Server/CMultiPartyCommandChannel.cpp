
#include <Server/CMultiPartyCommandChannel.h>
#include <Server/User.h>

namespace l2server {

void CMultiPartyCommandChannel::SendRelationUpdates()
{
	xstd::vector<User*> members = GetAllMember();
	for (xstd::vector<User*>::iterator i = members.begin() ; i != members.end() ; ++i) {
		(*i)->SendRelationChanged(0);
	}
}

xstd::vector<User*> CMultiPartyCommandChannel::GetAllMember()
{
	xstd::vector<User*> result;
	reinterpret_cast<void(*)(CMultiPartyCommandChannel*, xstd::vector<User*>*)>(0x76AE64)(this, &result);
	return result;
}

} // namespace l2server

