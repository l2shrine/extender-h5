
#pragma once

#include <Server/CIOObject.h>
#include <Server/CCursedWeaponConstInfo.h>

namespace l2server {

class CCursedWeaponMgr : public CIOObject {
public:
	static CCursedWeaponMgr* Instance();

	/* 0x0018 */ unsigned char padding0x0018[0x0058 - 0x0018];
	/* 0x0058 */ CCursedWeaponConstInfo constInfo;
};

} // namespace l2server

