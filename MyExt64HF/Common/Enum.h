
#pragma once

// Inventory slots
enum SlotType {
	SlotTypeUnderwear	= 0,
	SlotTypeREar		= 1,
	SlotTypeLEar		= 2,
	SlotTypeNeck		= 3,
	SlotTypeRFinger		= 4,
	SlotTypeLFinger		= 5,
	SlotTypeHead		= 6,
	SlotTypeRHand		= 7,
	SlotTypeLHand		= 8,
	SlotTypeGloves		= 9,
	SlotTypeChest		= 10,
	SlotTypeLegs		= 11,
	SlotTypeFeet		= 12,
	SlotTypeBack		= 13,
	SlotTypeLRHand		= 14,
	SlotTypeOnePiece	= 15,
	SlotTypeHair		= 16,
	SlotTypeAllDress	= 17,
	SlotTypeHair2		= 18,
	SlotTypeHairAll		= 19,
	SlotTypeRBracelet	= 20,
	SlotTypeLBracelet	= 21,
	SlotTypeDeco1		= 22,
	SlotTypeDeco2		= 23,
	SlotTypeDeco3		= 24,
	SlotTypeDeco4		= 25,
	SlotTypeDeco5		= 26,
	SlotTypeDeco6		= 27,
	SlotTypeWaist		= 28
};

// Chat types
enum ChatType {
	ChatTypeAll					= 0,
	ChatTypeShout				= 1,
	ChatTypeTell				= 2,
	ChatTypeParty				= 3,
	ChatTypeClan				= 4,
	ChatTypeGM					= 5,
	ChatTypePetitionPlayer		= 6,
	ChatTypePetitionGM			= 7,
	ChatTypeTrade				= 8,
	ChatTypeAlliance			= 9,
	ChatTypeAnnouncement		= 10,
	ChatTypeBoat				= 11,
	ChatTypeFriend				= 12,
	ChatTypeMSN					= 13,
	ChatTypePartymatchRoom		= 14,
	ChatTypePartyroomCommander	= 15,
	ChatTypePartyroomAll		= 16,
	ChatTypeHeroVoice			= 17,
	ChatTypeCriticalAnnounce	= 18,
	ChatTypeScreenAnnounce		= 19,
	ChatTypeBattlefield			= 20,
	ChatTypeMPCCRoom			= 21
};

enum Class {
	ClassFighter			= 0,
	ClassWarrior			= 1,
	ClassGladiator			= 2,
	ClassWarlord			= 3,
	ClassKnight				= 4,
	ClassPaladin			= 5,
	ClassDarkAvenger		= 6,
	ClassRogue				= 7,
	ClassTreasureHunter		= 8,
	ClassHawkeye			= 9,
	ClassMage				= 10,
	ClassWizard				= 11,
	ClassSorcerer			= 12,
	ClassNecromancer		= 13,
	ClassWarlock			= 14,
	ClassCleric				= 15,
	ClassBishop				= 16,
	ClassProphet			= 17,
	ClassElvenFighter		= 18,
	ClassElvenKnight		= 19,
	ClassTempleKnight		= 20,
	ClassSwordsinger		= 21,
	ClassElvenScout			= 22,
	ClassPlainsWalker		= 23,
	ClassSilverRanger		= 24,
	ClassElvenMage			= 25,
	ClassElvenWizard		= 26,
	ClassSpellsinger		= 27,
	ClassElementarSummoner	= 28,
	ClassOracle				= 29,
	ClassElder				= 30,
	ClassDarkFighter		= 31,
	ClassPalusKnight		= 32,
	ClassShillienKnight		= 33,
	ClassBladedancer		= 34,
	ClassAssassin			= 35,
	ClassAbyssWalker		= 36,
	ClassPhantomRanger		= 37,
	ClassDarkMage			= 38,
	ClassDarkWizard			= 39,
	ClassSpellhowler		= 40,
	ClassPhantomSummoner	= 41,
	ClassShillienOracle		= 42,
	ClassShillienElder		= 43,
	ClassOrcFighter			= 44,
	ClassOrcRaider			= 45,
	ClassDestroyer			= 46,
	ClassOrcMonk			= 47,
	ClassTyrant				= 48,
	ClassOrcMage			= 49,
	ClassOrcShaman			= 50,
	ClassOverlord			= 51,
	ClassWarcryer			= 52,
	ClassDwarvenFighter		= 53,
	ClassScavenger			= 54,
	ClassBountyHunter		= 55,
	ClassArtisan			= 56,
	ClassWarsmith			= 57,
	ClassDuelist			= 88,
	ClassDreadnought		= 89,
	ClassPhoenixNight		= 90,
	ClassHellKnight			= 91,
	ClassSagittarius		= 92,
	ClassAdventurer			= 93,
	ClassArchmage			= 94,
	ClassSoultaker			= 95,
	ClassArcanaLord			= 96,
	ClassCardinal			= 97,
	ClassHierophant			= 98,
	ClassEvasTemplar		= 99,
	ClassSwordMuse			= 100,
	ClassWindRider			= 101,
	ClassMoonlightSentinel	= 102,
	ClassMysticMuse			= 103,
	ClassElementalMuse		= 104,
	ClassEvasSaint			= 105,
	ClassShillienTemplar	= 106,
	ClassSpectralDancer		= 107,
	ClassGhostHunter		= 108,
	ClassGhostSentinel		= 109,
	ClassStormScreamer		= 110,
	ClassSpectralMaster		= 111,
	ClassShillienSaint		= 112,
	ClassTitan				= 113,
	ClassGrandKhavatari		= 114,
	ClassDominator			= 115,
	ClassDoomcryer			= 116,
	ClassFortuneSeeker		= 117,
	ClassMaestro			= 118,
	ClassKamaelMSoldier		= 123,
	ClassKamaelFSoldier		= 124,
	ClassTrooper			= 125,
	ClassWarder				= 126,
	ClassBerserker			= 127,
	ClassMSoulBreaker		= 128,
	ClassFSoulBreaker		= 129,
	ClassArbalester			= 130,
	ClassDoombringer		= 131,
	ClassMSoulHound			= 132,
	ClassFSoulHound			= 133,
	ClassTrickster			= 134,
	ClassInspector			= 135,
	ClassJudicator			= 136
};

enum Gender {
	GenderFemale			= 1,
	GenderMale				= 0
};

enum AttackType {
	AttackTypeNone			= 0,
	AttackTypeSword			= 1,
	AttackTypeBlunt			= 2,
	AttackTypeDagger		= 3,
	AttackTypePole			= 4,
	AttackTypeFist			= 5,
	AttackTypeBow			= 6,
	AttackTypeEtc			= 7,
	AttackTypeDual			= 8,
	AttackTypeDualfist		= 9,
	AttackTypeFishingrod	= 10,
	AttackTypeRapier		= 11,
	AttackTypeCrossbow		= 12,
	AttackTypeAncientsword	= 13,
	AttackTypeFlag			= 14,
	AttackTypeDualdagger	= 15,
	AttackTypeOwnthing		= 16
};

enum MoveType {
};

enum SkillPumpStartType {
};

enum SkillPumpEndType {
};

enum DamageTypeEnum {
};

enum CHAR_PARAM_TYPE {
};

enum ChangeTargetReason {
	ChangeTargetReasonDefault = 0,
	ChangeTargetReasonPacket = 1,
	ChangeTargetReasonSkill = 2
};

enum ItemGetReason {
	ItemGetReasonUnknown = 0
};

enum PetAction {
	PetActionChangeMode = 0,
	PetActionAttack = 1,
	PetActionStop = 2,
	PetActionGetItem = 3,
	PetActionSkill = 4,
	PetActionDummy = 5,
	PetActionMove = 6
};

enum AttributeTypeEnum {
	AttributeTypeNone = -2,
	AttributeTypeFire = 0,
	AttributeTypeWater = 1,
	AttributeTypeWind = 2,
	AttributeTypeEarth = 3,
	AttributeTypeHoly = 4,
	AttributeTypeUnholy = 5,
	AttributeTypeMax = 6
};

enum CrystalType {
	CrystalTypeNone = 0,
	CrystalTypeD = 1,
	CrystalTypeC = 2,
	CrystalTypeB = 3,
	CrystalTypeA = 4,
	CrystalTypeS = 5,
	CrystalTypeS80 = 6,
	CrystalTypeS84 = 7,
	CrystalTypeEvent = 8,
	CrystalTypeCrystalFree = 9
};

enum NavitPointChangeType {
};

enum PrivateStoreType {
	PrivateStoreTypeNone = 0,
	PrivateStoreTypeSell = 1,
	PrivateStoreTypeSellSet = 2,
	PrivateStoreTypeBuy = 3,
	PrivateStoreTypeBuySet = 4,
	PrivateStoreTypeManufacture = 5,
	PrivateStoreTypeManufactureSet = 6,
	PrivateStoreTypeUnused = 7,
	PrivateStoreTypePackageSell = 8,
	PrivateStoreTypePackageSellSet = 9
};

enum Country {
	COUNTRY_KOREA = 0,
	COUNTRY_USA = 1,
	COUNTRY_JAPAN = 2,
	COUNTRY_TAIWAN = 3,
	COUNTRY_CHINA = 4,
	COUNTRY_THAILAND = 5,
	COUNTRY_PHILIPPINES = 6,
	COUNTRY_INDONESIA = 7,
	COUNTRY_RUSSIA = 8
};

enum EtcSkillAcquireType {
	EtcSkillAcquireTypeNormal = 0,
	EtcSkillAcquireTypeFishing = 1,
	EtcSkillAcquireTypePledge = 2,
	EtcSkillAcquireTypeSubPledge = 3,
	EtcSkillAcquireTypeTransform = 4,
	EtcSkillAcquireTypeSubjob = 5,
	EtcSkillAcquireTypeCollect = 6,
	EtcSkillAcquireTypeBishopSharing = 7,
	EtcSkillAcquireTypeElderSharing = 8,
	EtcSkillAcquireTypeSilenElderSharing = 9
};

enum SkillAffectObjectTypeEnum {
	SkillAffectObjectTypeNone = 0,
	SkillAffectObjectTypeFriend = 1,
	SkillAffectObjectTypeNotFriend = 2,
	SkillAffectObjectTypeUndeadRealEnemy = 3,
	SkillAffectObjectTypeWyvernObject = 4,
	SkillAffectObjectTypeAll = 5,
	SkillAffectObjectTypeClan = 6,
	SkillAffectObjectTypeObjectDeadNpcBody = 7,
	SkillAffectObjectTypeInvisible = 8,
	SkillAffectObjectTypeHiddenPlace = 9
};

enum ObjectFieldType {
	ObjectFieldTypeAdditionalMakeList = 108,
	ObjectFieldTypeCorpseMakeList = 142,
	ObjectFieldTypeAdditionalMakeMultiList = 215,
	ObjectFieldTypeExItemDropList = 248
};

namespace GAME_EVENT {

enum EVENT_TYPE {
	VOTE_SYSTEM = 9
};

}; // namespace GAME_EVENT

namespace CDBExtPacket {

enum {
	REQUEST_TEST = 0,
	REPLY_TEST = 0,
	REQUEST_GET_PREMIUM = 1,
	REPLY_GET_PREMIUM = 1,
	REQUEST_SET_PREMIUM = 2,
	REPLY_SET_PREMIUM = 2
};

} // namespace CDBExtPacket

namespace NpcPacket {

enum {
	REMOVE_HATE = 0x2A,
	PET_ACTION = 0x37,
	DO_NOTHING = 0x3B
};

};

namespace NpcExtPacket {

enum {
	RIDE_WYVERN2 = 0,
	CLEAR_CONTRIBUTE_DATA = 1,
	SET_PREMIUM = 2,
	BROADCAST_ANNOUNCE = 3,
	BROADCAST_RED_SKY = 4
};

} // namespace NpcExtPacket

