/*
   Wednesday, September 25, 201915:14:45
   User: 
   Server: ELBERETH\SQLEXPRESS
   Database: h5_lin2world
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.premium_account
	(
	account_id int NOT NULL,
	expire_time int NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.premium_account ADD CONSTRAINT
	PK_premium_account PRIMARY KEY CLUSTERED 
	(
	account_id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.premium_account SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
